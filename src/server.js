const express = require('express');
const app = express();

const logger = require('./api/utils/logger');

require('./api/orm').makeRel()
    .then(require('./api/orm').updateSchema)
    .then(()=> {
        require('./api/filters/filters')(app);
        require('./api/controllers/controllers')(app);
        app.listen(3000, async function () {
            console.log('API listening on port 3000' + '\n' /*+ app.help*/);
        });
    });

