class AuthenticatedContent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var hash = window.location.hash.slice(1).replace(/\?.*/, '');
        if ("/bouteilles" === hash) {
            return (<BouteillesListe />);
        } else if ("/bouteilles/ajouter" === hash) {
            return (<BouteillesCreate/>);
        } else if (hash.match(/bouteilles\/([0-9]*)$/)) {
            return (<BouteillesDetail id={hash.replace("/bouteilles/", "")}/>);
        } else if (hash.match(/bouteilles\/([0-9]*)\/edit$/)) {
            return (<BouteillesUpdate id={hash.replace("/bouteilles/", "").replace("/edit", "")}/>);
        } else if ("/repartition" === hash) {
            return (<Repartition/>);
        } else if ("/emplacements" === hash) {
            return (<Emplacements/>);
        } else if ("/admin/mdp" === hash) {
            return (<Mdp/>);
        } else if ("/admin/casiers" === hash) {
            return (<Casiers/>);
        } else if ("/admin/parametres" === hash) {
            return (<Parametres/>);
        } else if ("/test" === hash) {
            return (<Test/>);
        } else {
            return (<BouteillesListe/>);
        }
    }

}

class Authenticated extends React.Component {
    render() {
        return (
            <div>
                <Menu page={window.location.hash.replace(/^#/, '').replace(/^\//, '').replace(/\?.*/, '').replace(/\//g, '-')}/>
                <AuthenticatedContent />
            </div>
        )
    }

}

const init = () => {
    Ajax.get("/api/configs/lang").then(async lang => {
        if (lang) {
            Cookies.set("lang", lang);
        } else {
            let lang = (window.navigator.userLanguage || window.navigator.language || "fr").replace(/(-|_)(.*)/, '').toLowerCase();
            Cookies.set("lang", lang);
            await Ajax.put("/api/configs", {lang: lang});
        }
    });
}

const dispatch = () => {
    ReactDOM.render(React.createElement(Authenticated, null), document.getElementById('root'));
};
