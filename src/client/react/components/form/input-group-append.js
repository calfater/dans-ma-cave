class InputGroupAppend extends React.Component {
    render() {
        return (
            <div className="input-group-append">
                <div className="input-group-text">{I18n.t(this.props.label)}</div>
            </div>
        );
    }
}

