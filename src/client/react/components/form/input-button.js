class InputButton extends React.Component {
    render() {
        return (
            <button onClick={evt => {this.props.onClick && this.props.onClick(evt)}} type="submit" className={"btn btn-" + (this.props.className || "primary")} type="submit"><I18n>{this.props.children}</I18n></button>
        );
    }
}
