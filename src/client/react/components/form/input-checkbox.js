class InputCheckbox extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = (evt)=> {
            if (this.props.onChange) {
                this.props.onChange(evt.target.checked, evt);
            }
        };
    }

    render() {
        return (
            <input checked={this.props.value} className={"form-control" + (this.props.width ? (" col-sm-" + this.props.width) : "")} id={this.props.id} name={this.props.id} value={this.props.value || false} type="checkbox" onChange={this.onChange}/>
        );
    }
}
