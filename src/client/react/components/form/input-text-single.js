class InputTextSingle extends React.Component {

    render() {
        return (
            <FormGroup>
                <FormLabel mandatory={this.props.mandatory} id={this.props.id} label={this.props.label}/>
                <Column width={this.props.width || 10}>
                    <InputText type={this.props.type} id={this.props.id} label={this.props.label} placeholder={this.props.placeholder} onChange={this.props.onChange} valid={this.props.valid} validator={this.props.validator} value={this.props.value} values={this.props.values}/>
                </Column>
            </FormGroup>
        )
    }
}
