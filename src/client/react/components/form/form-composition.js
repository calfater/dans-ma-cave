class FormComposition extends React.Component {

    render() {
        return (
            <FormGroup id={this.props.id} label={this.props.label}>
                <FormLabel id={this.props.id} label={this.props.label} />
                <InputSelect id={this.props.id} label={this.props.label} onChange={this.props.onChange} values={this.props.values} value={this.props.value}/>
            </FormGroup>
        )
    }
}
