class InputSelect extends React.Component {
    render() {
        return (
            <select className={"form-control" + (this.props.width ? (" col-sm-" + this.props.width) : "")} value={JSON.stringify(this.props.value)} onChange={evt => {this.props.onChange && this.props.onChange(JSON.parse(evt.target.value), evt)}} id={this.props.id} name={this.props.id}>
                {this.props.values.map((v, i)=><option key={i} value={JSON.stringify(v[0])}>{I18n.t(v[1])}</option>)}
            </select>
        );
    }
}
