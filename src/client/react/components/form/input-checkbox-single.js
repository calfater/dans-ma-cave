class InputCheckboxSingle extends React.Component {

    render() {
        return (
            <FormGroup>
                <FormLabel mandatory={this.props.mandatory} id={this.props.id} label={this.props.label}/>
                <Column width={this.props.width || 10}>
                    <InputCheckbox id={this.props.id} onChange={this.props.onChange} value={this.props.value} />
                </Column>
            </FormGroup>
        )
    }
}
