class FormGroup extends React.Component {
    render(input) {
        return (
            <div className="form-group form-row">
                {this.props.children}
            </div>
        );
    }
}

