class FormLabel extends React.Component {
    render() {
        return (<label htmlFor={this.props.id} className={"col-sm-"+ (this.props.width ? this.props.width : 2) +" col-form-label" + (this.props.mandatory ? " mandatory" : "")}>{I18n.t(this.props.label)}</label>);
    }
}
