class InputGroup extends React.Component {
    render() {
        return (
            <div  className={"input-group" + (this.props.width ? (" col-sm-" + this.props.width) : "")}>
                {this.props.children}
            </div>
        );
    }
}

