class InputText extends React.Component {
    constructor(props) {
        super(props);
        this.validate = (evt) => {
            $(evt.target).removeClass('alert alert-danger');
            var validator = this.props.validator;
            var val = $(evt.target).val();
            var valid = true;
            if (validator) {
                valid = validator(val);
            }
            if (!valid) {
                $(evt.target).addClass('alert alert-danger');
            }
        };

        this.onChange = (evt) => {
            if (this.props.onChange) {
                if ('numeric' === evt.target.getAttribute('type')) {
                    this.props.onChange(parseInt(evt.target.value), evt);
                } else {
                    this.props.onChange(evt.target.value, evt);
                }
            }
        };
        this.addAutoComplete = () => {
            var ctx = this;
            if (this.props.values) {
                $('#' + ctx.props.id).autocomplete({
                    minLength: 0,
                    autoFocus: true,
                    select: function (event, ui) {
                      ctx.onChange(event);
                    },
                    source: (request, response) => {
                        var search = request.term;
                        var match = [];
                        for (let value of ctx.props.values) {
                            if (tidy(value).includes(tidy(search)) || " " === search) {
                                match.push(value);
                            }
                        }
                        response(match);
                    }
                });
            }
        }
    }

    componentDidMount() {
        this.addAutoComplete();
    }

    componentDidUpdate() {
        this.addAutoComplete();
    }

    render() {
        return (
            <input
                className={"form-control" + (this.props.width ? (" col-sm-" + this.props.width) : "")}
                id={this.props.id}
                name={this.props.id}
                placeholder={I18n.t(this.props.placeholder)}
                onBlur={evt => {this.validate(evt); this.onChange(evt)}} value={this.props.value || ''}
                type={this.props.type || "text" }
                min={this.props.min ? this.props.min : "" }
                max={this.props.max ? this.props.max : "" }
                step={this.props.step ? this.props.step : 1}
                onChange={evt => this.onChange(evt)}
                autoComplete="off"
            />
        );
    }
}
