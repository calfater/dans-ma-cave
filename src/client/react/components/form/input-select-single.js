class InputSelectSingle extends React.Component {

    render() {
        return (
            <FormGroup>
                <FormLabel mandatory={this.props.mandatory} id={this.props.id} label={this.props.label}/>
                <Column width={this.props.width || 10}>
                    <InputSelect id={this.props.id} label={this.props.label} onChange={this.props.onChange} values={this.props.values} value={this.props.value}/>
                </Column>
            </FormGroup>
        )
    }
}
