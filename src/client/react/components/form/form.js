class Form extends React.Component {
    render() {
        return (<form className="form" onSubmit={evt => {this.props.onSubmit && this.props.onSubmit(evt)}}>
            {this.props.children}
        </form>);
    }
}
