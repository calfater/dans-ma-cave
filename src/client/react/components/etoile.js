// 0 -> Vraiment null
// 1 -> Tout venant buvable en soirée
// 2 -> Bouteille du caviste
// 3 -> Tres bonnes a conserver
// 4 -> Vieilles de chez mes parents
// 5 -> Exceptionnel (genre 18 ans d'Aurèle)

class Etoile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {maxStarsCount: 5};

        this.html = () => {
            var etoiles = [];
            var value = this.props.value || 0;

            for (let i = 1; i <= this.state.maxStarsCount; ++i) {
                etoiles.push(<span onClick={(evt) => {if (!this.props.onChange) {return;} evt.preventDefault(); evt.stopPropagation(); this.props.onChange(i)}} key={"etoile_" + i}><i className={i <= value ? "fa fa-star" : "fa fa-star-o"}></i></span>);
            }

            return (
                <div className="etoile" onClick={() => {if (!this.props.onChange) {return;} this.props.onChange(0);}}>
                    {etoiles}
                </div>
            );
        };

    }

    render() {
        return this.html();
    }
}


