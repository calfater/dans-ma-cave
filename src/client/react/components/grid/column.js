class Column extends React.Component {
    render() {
        return (
            <div className={"col-sm-"+ (this.props.width || "12")}>
                {this.props.children}
            </div>
        );
    }
}

