class ProgressBar extends React.Component {
    render() {
        return (
            <div className="progress">
                <div className={"progress-bar" + (" w-" + (this.props.width || "0")) + (" progress-bar-color-" + (this.props.width || "0"))}
                     role="progressbar" aria-valuenow={this.props.width || 0} aria-valuemin="0" aria-valuemax="100">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

