class Table2Cols extends React.Component {
    constructor(props) {
        super(props);

        this.state = {orderBy: props.defaultSort || "value", way: props.defaultSort ? 'asc' : "desc"};

        this.updateSortParams = (orderBy) => {
            var way = (orderBy === 'value') ? 'desc' : 'asc';
            if (this.state.orderBy === orderBy) {
                way = ('asc' === this.state.way ? 'desc' : 'asc')
            }
            this.props.onSort(orderBy, way);
            this.setState({way: way, orderBy: orderBy});
        };

    }

    render() {
        return (
            <Table className="table-striped table-bordered">
                <TableHead>
                    <TableRow>
                        <ThSortable sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} by="label" label={this.props.label}/>
                        <ThSortable sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} by="value" label="quantite" type="numeric"/>
                    </TableRow>
                </TableHead>
                <tbody>
                {this.props.data && this.props.data.map(entry => {
                    let label = entry.label;
                    let value = entry.value;
                    var filter = {};
                    filter["region"] = label;
                    return (
                        <tr key={label} className="filter" onClick={() => {this.props.onClick(label)}}>
                            <td>{label}</td>
                            <td>{value}</td>
                        </tr>
                    )
                }) }
                </tbody>
            </Table>
        );
    }
}

