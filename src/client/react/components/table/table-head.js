class TableHead extends React.Component {
    render() {
        return (
            <thead className="thead-light">
            {this.props.children}
            </thead>
        );
    }
}

