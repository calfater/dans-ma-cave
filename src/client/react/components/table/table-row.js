class TableRow extends React.Component {
    render() {
        return (
            <tr className={this.props.className}>
                {this.props.children}
            </tr>
        );
    }
}

