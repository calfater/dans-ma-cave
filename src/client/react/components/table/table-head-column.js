class TableHeadColumn extends React.Component {
    render() {
        return (
            <th className={this.props.className} onClick={(evt) => {this.props.onClick && this.props.onClick(evt);}} title={this.props.title}>
                {this.props.children }
            </th>
        );
    }
}

