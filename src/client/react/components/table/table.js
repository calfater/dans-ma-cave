class Table extends React.Component {
    render() {
        return (
            <table className={"table table-sm" + (this.props.className && (" " + this.props.className))}>
                {this.props.children}
            </table>
        );
    }
}

