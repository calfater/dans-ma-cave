class TableBody extends React.Component {
    render() {
        return (
            <tbody className="table-striped">
            {this.props.children}
            </tbody>
        );
    }
}

