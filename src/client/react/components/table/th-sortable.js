class ThSortable extends React.Component {
    constructor(props) {
        super(props);
        this.order = () => {
            if (this.props.by === this.props.orderBy) {
                if (this.props.way === 'asc') {
                    return ('asc');
                } else {
                    return ('desc');
                }
            } else {
                return '';
            }
        };

        this.sortClassName = () => {
          var className = "fa ";
          if (this.order()) {
              className += "fa fa-sort-";
              className += this.props.type || 'alpha';
              className += "-";
              if (this.order() === 'asc') {
                  className += 'asc';
              } else {
                className += 'desc';
              }
          }
          return className;
        }
    }

    render() {
        return (
            <th className="sortableHeader" onClick={() => {this.props.sort(this.props.by)}}>
                <span className="d-none d-lg-block">
                    <i className={this.sortClassName()}> </i>
                    {I18n.t(this.props.label)}
                </span>
                <span className="d-lg-none">
                    <i className={this.sortClassName()}> </i>
                    {I18n.t(this.props.label + " court")}
                </span>
            </th>
        );
    }
}
