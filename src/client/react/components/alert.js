class Alert extends React.Component {
    render() {
        return (
            <div className={"alert alert-"+(this.props.type || 'primary')} role="alert">
                {this.props.children}
            </div>
        );
    }
}

