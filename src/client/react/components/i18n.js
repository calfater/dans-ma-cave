class I18n extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var key = this.props.k || this.props.children;
        var def = this.props.children || this.props.k;

        return I18n.t(key, this.props, def);

    }
}

I18n.t = (key, replacments, def) => {
    let str = def || key;
    if (null == str || undefined == str || 'object' == typeof(str)) {
        return str;
    }
    str = "" + str;
    let lang = Cookies.get("lang");
    if (I18n.i18nConfig[lang] && I18n.i18nConfig[lang][key]) {
        str = (I18n.i18nConfig[lang][key]);
    } else if (I18n.i18nConfig._[key]) {
        str = (I18n.i18nConfig._[key]);
    }
    str = str.replace(/(\[.*?])/g, match => undefined != replacments[match.replace(/\[|]/g, '')] ? replacments[match.replace(/\[|]/g, '')] : match);
    return str;
};

