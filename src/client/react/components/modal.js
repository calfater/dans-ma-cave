class Modal extends React.Component {
    render() {
        return (
            <div className="modal fade" id={this.props.id} role="dialog" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">{this.props.title}</h5>
                            <button type="button" className="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {this.props.children}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">{this.props.dismissLabel || "Non"}</button>
                            {this.props.secondaryClick
                                ? <button type="button" className="btn btn-success" onClick={(evt) => this.props.secondaryClick(evt)}>{this.props.secondaryLabel || "Secondary"}</button>
                                : ""
                            }
                            <button type="button" className="btn btn-primary" onClick={(evt) => this.props.primaryClick(evt)}>{this.props.primaryLabel || "Primary"}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}