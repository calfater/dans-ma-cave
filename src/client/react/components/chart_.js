class Chart_ extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        if (this.props.data && this.props.data.length) {

            var ctx = document.getElementById(this.props.id);
            var labels = [];
            var datas = [];

            this.props.data.map((a) => {
                labels.push(a.label);
                datas.push(a.value);
            });
            this.state.chart && this.state.chart.destroy();
            this.state.chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [
                        {
                            data: datas
                        }
                    ]

                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                precision: 0
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                beginAtZero: true,
                                autoSkip: false
                            },
                            gridLines: {
                                color: "rgba(0,0,0,0)"
                            }
                        }]
                    },
                    tooltips: {
                        enabled: false
                    },
                    onClick: (_, info) => {
                        var x = info[0] && labels[info[0]._index];
                        if (undefined !== x) {
                            this.props.onClick && this.props.onClick(x);
                        }
                    },
                    hover: {
                        onHover: function (e, el) {
                            $(ctx).css("cursor", el[0] ? "pointer" : "default");
                        }
                    }
                }
            });

        }

        return (
            <canvas id={this.props.id}></canvas>
        );
    }
}

