class Validators {
}

Validators.string = (options) => {
    var min = options.min;
    var required = 'undefined' !== typeof (options.required) ? options.required : false;
    return (val) => {
        var ok = true;
        if (!required && !val) {
            return true;
        }
        if ('undefined' !== typeof(min)) {
            ok = ok && val.length >= min;
        }
        if ('undefined' !== typeof(max)) {
            ok = ok && val.length <= max;
        }
        return ok;
    }
};

Validators.number = (options) => {
    var min = options.min;
    var max = options.max;
    var required = 'undefined' !== typeof (options.required) ? options.required : false;
    return (val) => {
        var ok = true;
        if (!required && !val) {
            return true;
        }
        if (required && !val && 0 !== val) {
            ok = false;
        }
        if ('undefined' !== typeof(min)) {
            ok = ok && val >= min;
        }
        if ('undefined' !== typeof(max)) {
            ok = ok && val <= max;
        }
        return ok;
    }
};
