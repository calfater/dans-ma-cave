class Url {
}

Url.getHashUrl = () => {
    return window.location.hash.slice(1).replace(/\?.*/, '');
};

Url.getHashParams = () => {
    var params = [];
    if (window.location.hash.includes("?")) {
        for (let pair of window.location.hash.slice(1).replace(/.*\?/, '').split('&')) {
            var s = pair.split('=');
            params[s[0]] = decodeURI(s[1]);
        }
    }
    return params;
};

Url.getHashParam = (name) => {
    return Url.getHashParams()[name];
};

Url.addHashParam = (key, value) => {
    var url = Url.getHashUrl();
    var params = Url.getHashParams();
    params[key] = value;
    Url.goTo(url, params);
};

Url.goTo = (url, params) => {
    params = params || {};
    var to = "";
    to += url;
    to += "?";
    Object.entries(params).map(entry => {
        to += entry[0] + "=" + entry[1] + "&";
    });
    window.location.hash = to.slice(0, to.length - 1);
};