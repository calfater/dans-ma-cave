var tidy = (str) => {
    if ('string' !== typeof(str)) {
        return str;
    } else {
        return str.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").trim();
    }
};
