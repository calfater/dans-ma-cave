var sortObj = function (a, b, orderBy, way) {
    way = way || 'asc';

    var cmp = 0;
    var a_v = a;
    var b_v = b;

    for (let prop of orderBy.split('.')) {
        a_v = a_v ? a_v[prop] : null;
        b_v = b_v ? b_v[prop] : null;
    }
    a_v = tidy(a_v);
    b_v = tidy(b_v);

    if ((null === a_v || 'undefined' === typeof(a_v)) && (null !== b_v && 'undefined' !== typeof(b_v))) {
        cmp = 1;
    } else if ((null !== a_v && 'undefined' !== typeof(a_v)) && (null === b_v || 'undefined' === typeof(b_v))) {
        cmp = -1;
    } else if (a_v > b_v) {
        cmp = 1;
    } else if (a_v < b_v) {
        cmp = -1;
    }
    return ('asc' !== way) ? -cmp : cmp;

};
