class StringUtils {
}
StringUtils.enjoliveLeNom = nom => {
    return StringUtils.capitalizeFirst(nom.replace(/^(( *)(((ch(a|â)teau)|(domaine))( +))((d')*)(((de|du|des|le|la|les)( +))*))(.*)/gi, "$15 ($1)").replace(" )", ")"));
};

StringUtils.capitalizeFirst = str => str && str.replace(str[0], str[0].toUpperCase());

StringUtils.romanize = val => {
    var ret = null;
    if (("" + parseInt(val)) === ("" + val) && ("" + val) !== "0") { // c'est un entier, on le convertit
        ret = String.fromCharCode(65 + val - 1);
    } else if (val) { // Ce n'est pas un entier, on le capitalize
        ret = val.toUpperCase();
    }
    //console.log('romanize', val, "=>", ret);
    return ret;
};

StringUtils.unRomanize = val => {
    var ret = null;
    if (("" + parseInt(val)) === ("" + val)) { // c'est un entier, on le retourne
        ret = parseInt(val);
    } else if (val) {
        ret = val.toUpperCase().charCodeAt(0) - 65 + 1;
    }
    //console.log('unRomanize', val, "=>", ret);
    return ret;
};
