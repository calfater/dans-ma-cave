class Ajax {
}

Ajax.get = (where) => {
    return $.ajax({
        url: where,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json; charset=utf-8'
    });
};

Ajax.post = (where, what) => {
    return $.ajax({
        url: where,
        type: 'POST',
        data: JSON.stringify(what),
        dataType: "json",
        contentType: 'application/json; charset=utf-8'
    });
};

Ajax.put = (where, what) => {
    return $.ajax({
        url: where,
        type: 'PUT',
        data: JSON.stringify(what),
        dataType: "json",
        contentType: 'application/json; charset=utf-8'
    });
};

Ajax.delete = (where) => {
    return $.ajax({
        url: where,
        type: 'DELETE'
    })
};
