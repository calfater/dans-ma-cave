class EmplacementCasierBouteille extends React.Component {
    constructor(props) {
        super(props);

        this.consommer = (evt) => {
            evt.preventDefault();
            evt.stopPropagation();
            Ajax.delete("/api/bouteilles/" + this.props.bouteille.id + "/emplacements/" + this.props.bouteille.emplacementId)
                .then(() => new Promise(resolve => {
                    $("#consommer_" + this.props.bouteille.emplacementId).modal('hide');
                    resolve();
                }))
                .then(this.props.onDelete);
        };

        this.consulter = (evt) => {
            evt.preventDefault();
            evt.stopPropagation();
            $("#consommer_" + this.props.bouteille.emplacementId).modal('hide');
            Url.goTo("/bouteilles/" + this.props.bouteille.id);
        };

    }

    render() {
        return (<div className={"casier-detail-bouteille" + (this.props.bouteille ? " present" : "") } onClick={()=> {this.props.bouteille && $("#consommer_" + this.props.bouteille.emplacementId).modal()}}>
            <span className="nom">{this.props.bouteille && this.props.bouteille.nom}&nbsp;</span>
            <span className="region">{this.props.bouteille && this.props.bouteille.region}&nbsp;</span>
            <span className="appellation">{this.props.bouteille && this.props.bouteille.appellation}&nbsp;</span>
            <span className="type">{this.props.bouteille && this.props.bouteille.type}&nbsp;</span>
            <span className="millesime">{this.props.bouteille && this.props.bouteille.millesime}&nbsp;</span>

            {this.props.bouteille ?
                <Modal id={"consommer_" + this.props.bouteille.emplacementId}
                       title=""
                       dismissLabel="Annuler"
                       primaryLabel="Retirer" primaryClick={this.consommer}
                       secondaryLabel="Consulter" secondaryClick={this.consulter}>
                    <p>Voulez vous consulter {this.props.bouteille.nom} ou retirer la bouteille ?</p>
                </Modal>
                : "" }
        </div>);
    }
}
