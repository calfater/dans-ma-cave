class Emplacements extends React.Component {
    constructor(props) {
        super(props);
        this.state = {casiers: [], form: {}};

        Ajax.get("/api/casiers")
            .then(data => new Promise(resolve => {
                    this.state.casiers = [];
                    data.sort((a, b) => sortObj(a, b, "nom")).map((casier)=> {
                        this.state.casiers.push([casier, casier.nom])
                    });
                    this.state.form.casier = data[0];
                    this.forceUpdate();
                })
            )
    }

    componentDidUpdate() {
    }

    render() {

        return (
            <div className="emplacement">
                <InputSelectSingle label="Casier" values={this.state.casiers} value={this.state.form.casier} onChange={(value)=> {this.state.form.casier=value; this.forceUpdate();}}/>
                <EmplacementCasier casier={this.state.form.casier}/>
            </div>
        );
    }
}
