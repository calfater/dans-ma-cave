class EmplacementCasier extends React.Component {
    constructor(props) {
        super(props);
        this.state = {bouteillesParCasier: {}}

        this.colonnes = (lineIdx) => {
            var a = [];
            a.push(<th key="_">{StringUtils.romanize(lineIdx)}</th>);
            for (var colonneIdx = 1; colonneIdx <= this.props.casier.nombre_de_colonnes; colonneIdx++) {
                a.push(<td key={colonneIdx}><EmplacementCasierBouteille bouteille={this.state.bouteillesParCasier[colonneIdx] && this.state.bouteillesParCasier[colonneIdx][lineIdx]} onDelete={this.callWsAndUpdate}/></td>);
            }
            return (a);
        };

        this.lignes = () => {
            var a = [];
            if (this.props.casier) {
                for (var lineIdx = 1; lineIdx <= this.props.casier.nombre_de_lignes; lineIdx++) {
                    a.push(
                        <tr key={lineIdx}>
                            {this.colonnes(lineIdx)}
                        </tr>
                    );
                }
            }
            return (<tbody>{a}</tbody>)
        };

        this.header = () => {
            var a = [];
            if (this.props.casier) {
                for (var colonneIdx = 1; colonneIdx <= this.props.casier.nombre_de_colonnes; colonneIdx++) {
                    a.push(<th key={colonneIdx}>{colonneIdx}</th>);
                }
            }
            return (
                <TableHead>
                    <TableRow>
                        <TableColumn></TableColumn>
                        {a}
                    </TableRow>
                </TableHead>)
        };
        this.callWsAndUpdate = () => {
            Ajax.get("/api/casiers/" + this.props.casier.id + "/bouteilles")
                .then(data => new Promise(resolve => {
                    this.state.bouteillesParCasier = data;
                    this.forceUpdate();
                    resolve();
                }));
        };

    }

    componentDidUpdate() {

        if (this.props.casier) {
            if (this.props.casier !== this.state.casierOld) {
                this.state.casierOld = this.props.casier;
                this.callWsAndUpdate();
            }
        }
    }

    render() {
        if (this.props.casier) {
            return (
                <table id={'casier' + (this.props.casier.id )} className="">
                    {this.header()}
                    {this.lignes()}
                </table>
            );
        } else {
            return (<Table></Table>)
        }
    }
}
