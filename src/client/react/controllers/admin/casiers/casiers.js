class Casiers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {casiers: []};

        this.updateCasier = (evt, casier) => {
            evt.preventDefault();
            if (null === casier.id) {
                Ajax.post('/api/casiers', this.state.casiers[this.state.casiers.length - 1]).then(this.init);
            } else {
                Ajax.put('/api/casiers/' + casier.id, casier).then(this.init)
            }
        };

        this.removeCasier = (evt, id) => {
            evt.preventDefault();
            Ajax.delete('/api/casiers/' + id).then(this.init)
        };

        this.init = () => {
            return $.get("/api/casiers").then(data => new Promise(resolve => {
                data.push({id: null, nom: "", nombre_de_colonnes: null, nombre_de_lignes: null});
                this.setState({casiers: data});
                resolve();
            }));
        };

        this.init();

    }

    render() {
        return (<div>
                {this.state.casiers.sort((a, b) => sortObj(a, b, 'id')).map((casier, idx) => {
                    return (
                        <Form key={idx}>
                            <Row>
                                <Column width="12">
                                    <FormGroup>
                                        <InputGroup width="4">
                                            <InputGroupPrepend label="nom"/>
                                            <InputText id="nom" value={casier.nom} onChange={(value)=> {casier.nom = value; this.forceUpdate()}} placeholder="Emplacement du casier"/>
                                        </InputGroup>
                                        <InputGroup width="3">
                                            <InputGroupPrepend label="colonnes"/>
                                            <InputText id="colonnes" value={casier.nombre_de_colonnes} onChange={(value)=> {casier.nombre_de_colonnes = value; this.forceUpdate()}} placeholder="casier colonne placeholder"/>
                                        </InputGroup>
                                        <InputGroup width="3">
                                            <InputGroupPrepend label="lignes"/>
                                            <InputText id="lignes" value={casier.nombre_de_lignes} onChange={(value)=> {casier.nombre_de_lignes = value; this.forceUpdate()}} placeholder="casier ligne placeholder"/>
                                        </InputGroup>
                                        <InputGroup width="1">
                                            <InputButton className={null === casier.id ? "success" : "primary"} onClick={(evt) => this.updateCasier(evt, casier)}><i className={"fa" + (null === casier.id ? " fa-plus" : " fa-upload")}></i></InputButton>
                                        </InputGroup>
                                        <InputGroup width="1">
                                            {null !== casier.id ?
                                                <InputButton onClick={(evt) => this.removeCasier(evt, casier.id)} className="danger"><i className="fa fa-minus"></i></InputButton>
                                                : ""}
                                        </InputGroup>
                                    </FormGroup>
                                </Column>
                            </Row>
                        </Form>
                    )
                })}
            </div>
        );
    }
}
