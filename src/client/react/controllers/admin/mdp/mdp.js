class Mdp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {login: null, oldPasswordMd5: null, newPassword: null, newPasswordCheck: null};

        this.submitPassword = (evt) => {
            evt.preventDefault();
            if (this.state.newPassword === this.state.newPasswordCheck
                && this.state.oldPasswordMd5 === md5(this.state.oldPasswordCheck)) {
                Ajax.put('/api/configs', {login: this.state.login, mot_de_passe: md5(this.state.newPassword)})
                    .then(()=> {
                        this.init();
                        this.setState({success: true, error: false})
                    });
            } else {
                this.setState({success: false, error: true})
            }

        };

        this.init = () => {
            return Ajax.get("/api/configs").then(data => new Promise(resolve => {
                this.state.login = data.login;
                this.state.oldPasswordMd5 = data.mot_de_passe;
                this.state.oldPasswordCheck = "";
                this.state.newPassword = "";
                this.state.newPasswordCheck = "";
                this.forceUpdate(resolve)
            }));
        };

        this.init();

    }

    render() {
        return (
            <form className="form" onSubmit={this.submitPassword}>
                <Row>
                    <Column width="3"/>
                    <Column width="6">
                        <FormGroup>
                            <FormLabel mandatory="true" width="4" id="login" label="login"/>
                            <InputText width="8" id="login" value={this.state.login} onChange={(value)=> this.setState({login: value})}/>
                        </FormGroup>
                    </Column>
                    <Column width="3"/>
                </Row>
                <Row>
                    <Column width="3"/>
                    <Column width="6">
                        <FormGroup>
                            <FormLabel id="oldPasswordCheckMd5" mandatory="true" width="4" label="ancien mot de passe"/>
                            <InputText id="oldPasswordCheckMd5" width="8" value={this.state.oldPasswordCheck} onChange={(value)=> {this.setState({oldPasswordCheck :value})}} type="password"/>
                        </FormGroup>
                        <Column width="3"/>
                    </Column>
                </Row>
                <Row>
                    <Column width="3"/>
                    <Column width="6">
                        <FormGroup>
                            <FormLabel id="motDePasse" mandatory="true" width="4" label="nouveau mot de passe"/>
                            <InputText id="motDePasse" width="8" value={this.state.newPassword} onChange={(value)=> {this.setState({newPassword : value})}} type="password"/>
                        </FormGroup>
                        <Column width="3"/>
                    </Column>
                </Row>
                <Row>
                    <Column width="3"/>
                    <Column width="6">
                        <FormGroup>
                            <FormLabel id="motDePasseCheck" mandatory="true" width="4" label="confirmer le mot de passe"/>
                            <InputText id="motDePasseCheck" width="8" value={this.state.newPasswordCheck} onChange={(value)=> this.setState({newPasswordCheck : value})} type="password"/>
                        </FormGroup>
                        <Column width="3"/>
                    </Column>
                </Row>
                <Row>
                    <Column width="3"/>
                    <Column width="2">
                        <button className="btn btn-primary"><I18n>enregistrer</I18n></button>
                    </Column>
                    <Column width="4">
                        {this.state.success ? <Alert type="success"><I18n>mot de passe change</I18n>.</Alert> : ""}
                        {this.state.error ?
                            <Alert type="danger"><I18n>erreur changement de mot de passe</I18n></Alert>
                            : ""}
                    </Column>
                    <Column width="3"/>
                </Row>
            </form>
        );
    }
}
