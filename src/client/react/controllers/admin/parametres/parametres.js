class Parametres extends React.Component {

    constructor(props) {
        super(props);

        this.state = {configs: {}};
        this.EXCLUDED_FIELDS = ["login", "mot_de_passe", "db_version"];

        this.submit = (evt) => {
            evt.preventDefault();
            Ajax.put("/api/configs", this.state.configs).then(() => {
                window.location.reload();
            });
        };

        Ajax.get("/api/configs").then(configs => new Promise(resolve => {
            this.state.configs = configs;
            for (let key of this.EXCLUDED_FIELDS) {
                delete this.state.configs[key];
            }
            this.forceUpdate(resolve);
        }));
    }

    render() {
        return (
            <Form onSubmit={this.submit}>
                {Object.entries(this.state.configs).map((config) => {
                    var key = config[0];
                    var value = config[1];
                    return <InputTextSingle value={value} key={key} label={I18n.t(key)} onChange={(value) => {this.state.configs[key] = value; this.forceUpdate();}}/>
                })}
                <InputButton>enregistrer</InputButton>
            </Form>
        );
    }
}
