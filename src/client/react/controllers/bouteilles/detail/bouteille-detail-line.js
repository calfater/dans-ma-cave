class BouteilleDetailLine extends React.Component {
    constructor(props) {
        super(props);
        this.getLine = (label, value) => {
            if ('undefined' === typeof(value) || null === value || "" === value || ('object' === typeof(value) && 0 === value.length)) {
                return (<span></span>);
            } else {
                return (
                    <dl className="row">
                        <dt className="col-sm-3 _text-right">{label} :</dt>
                        {this.getValue(value)}
                    </dl>

                );
            }
        };

        this.getValue = (value) => {
            if ('string' === typeof(value) || 'number' === typeof(value)) {
                return (<dd className="col-sm-9">{value}</dd>);
            } else if ('object' === typeof(value)) {
                return (
                    <dd className="col-sm-9">
                        <ul>
                            {value.map((v) => <li key={v}>{v}</li>)}
                        </ul>
                    </dd>);
            } else {
                return (<dd className="col-sm-9">Type '{typeof(value)}' non supporté</dd>);
            }
        }
    };

    render() {
        return (this.getLine(this.props.label, this.props.value));
    }
}
