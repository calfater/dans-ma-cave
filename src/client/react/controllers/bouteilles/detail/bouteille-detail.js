class BouteillesDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {bouteille: {type: {}, region: {}, volume: {}, emplacements: [], compositions: []}, popupConsommer: {}};

        this.delete = (evt) => {
            evt.preventDefault();
            evt.stopPropagation();
            if (confirm("Supprimer")) {
                Ajax.delete("/api/bouteilles/" + this.props.id)
                    .then(() => new Promise(resolve => {
                        window.location.hash = '#/bouteilles';
                        resolve();
                    }));
            }
        };

        this.consommerPopup = (evt, emplacementId, casier, colonne, ligne) => {
            evt.preventDefault();
            evt.stopPropagation();
            this.setState({
                popupConsommer: {
                    emplacementId: emplacementId,
                    bouteille: this.state.bouteille.nom,
                    casier: casier,
                    ligne: ligne,
                    colonne: colonne
                }
            });
            $("#consommer").modal();
        };

        this.consommer = (evt, emplacementId) => {
            evt.preventDefault();
            evt.stopPropagation();
            Ajax.delete("/api/bouteilles/" + this.state.bouteille.id + "/emplacements/" + this.state.popupConsommer.emplacementId)
                .then(() => new Promise(resolve => {
                    $("#consommer").modal('hide');
                    resolve();
                }))
                .then(this.init)
        };

        this.init = () => {
            return Ajax.get("/api/bouteilles/" + this.props.id)
                .then(bouteille => new Promise(resolve => {
                    this.setState({bouteille: bouteille});
                    resolve();
                }))
        };

        this.init();
    }

    render() {

        return (
            <div className="bouteilles-detail">
                <div className="btn-group action ">
                    <button type="button" className="menu-btn btn btn-link text-dark" data-toggle="dropdown">
                        <span className="fa fa-ellipsis-v caret"></span>
                    </button>
                    <ul className="dropdown-menu dropdown-menu-right" role="menu">
                        <li><a className="dropdown-item btn btn-link text-primary edit-link" href={"#/bouteilles/"+ this.state.bouteille.id + "/edit"}>Modifier</a></li>
                        <li>
                            <form onSubmit={this.delete}>
                                <button className="dropdown-item btn btn-link text-danger remove-link">Supprimer</button>
                            </form>
                        </li>
                    </ul>
                </div>

                <h1>{this.state.bouteille.nom}</h1>
                {this.state.bouteille.est_indicee ? <Column width="3"><ProgressBar width={this.state.bouteille.indice && this.state.bouteille.indice.valeur_pourcent}>{this.state.bouteille.indice && this.state.bouteille.indice.valeur_pourcent}</ProgressBar></Column> : ""}


                {this.state.bouteille.img
                    ? <div className="bouteille-image"><a href={this.state.bouteille.img || '/static/bouteille.svg'}><img src={this.state.bouteille.img || '/static/bouteille.svg'}/></a></div>
                    : "" }
                { /* <td><a href={"https://www.vitiplace.com/vpsearch.php?q=" + this.props.bouteille.nom}>{this.props.bouteille.nom}</a></td> */ }
                <BouteilleDetailLine label="Type" value={this.state.bouteille.type.nom}/>
                <BouteilleDetailLine label="Région" value={this.state.bouteille.region.nom}/>
                <BouteilleDetailLine label="Appellation" value={this.state.bouteille.appellation ? this.state.bouteille.appellation.nom : ""}/>
                <BouteilleDetailLine label="Millesime" value={this.state.bouteille.millesime}/>
                <BouteilleDetailLine label="A consommer à partir de" value={this.state.bouteille.boire_apres}/>
                <BouteilleDetailLine label="Apogée" value={this.state.bouteille.boire_apogee}/>
                <BouteilleDetailLine label="A consommer avant" value={this.state.bouteille.boire_avant}/>
                <BouteilleDetailLine label="Volume" value={this.state.bouteille.volume.volume && this.state.bouteille.volume.volume / 1000 + 'L'}/>

                <BouteilleDetailLine label="Stock" value={this.state.bouteille.stock}/>

                {this.state.bouteille.emplacements.length ?
                    <dl className="row">
                        <dt className="col-sm-3 _text-right">Emplacement :</dt>
                        <dd className="col-sm-9">
                            <ul className="emplacementList">
                                {this.state.bouteille.emplacements.map((emplacement) =>
                                    <li key={emplacement.id}>{emplacement.casier.nom} {emplacement.colonne ? (" [" + emplacement.colonne + "-" + StringUtils.romanize(emplacement.ligne) + "]") : ""}
                                        <button className="btn btn-link" onClick={(evt) => this.consommerPopup(evt, emplacement.id, emplacement.casier.nom, emplacement.colonne, emplacement.ligne)}><i className="fa fa-hand-grab-o"></i></button>
                                    </li>)
                                }
                            </ul>
                        </dd>
                    </dl>
                    : ""
                }

                <BouteilleDetailLine label="Prix" value={this.state.bouteille.prix && this.state.bouteille.prix + '€'}/>

                {this.state.bouteille.etoile ?
                    <dl className="row">
                        <dt className="col-sm-3 _text-right">Note :</dt>
                        <dd className="col-sm-9"><Etoile value={this.state.bouteille.etoile}/></dd>
                    </dl>
                    : ""
                }

                {this.state.bouteille.compositions.length ?
                    <dl className="row">
                        <dt className="col-sm-3 _text-right">Cépages</dt>
                        <dd className="col-sm-9">
                            <ul className="compositionList">
                                {this.state.bouteille.compositions.sort((a, b) => sortObj(a, b, 'pourcentage', 'desc')).map((composition) =>
                                    <li key={composition.id}>{composition.cepage.nom}{100 !== composition.pourcentage ? " (" + composition.pourcentage + "%)" : ""}</li>)}
                            </ul>
                        </dd>
                    </dl>
                    : ""
                }

                <BouteilleDetailLine label="Description" value={this.state.bouteille.description}/>

                <Modal id="consommer" title="Retirer" primaryClick={(evt) => this.consommer(evt)} primaryLabel="Oui">
                    <p>Retirer la bouteille {this.state.popupConsommer.bouteille} de {this.state.popupConsommer.casier} {this.state.popupConsommer.colonne ? (" [" + this.state.popupConsommer.colonne + "-" + StringUtils.romanize(this.state.popupConsommer.ligne) + "]") : ""}</p>
                </Modal>

            </div>
        );
    }
}
