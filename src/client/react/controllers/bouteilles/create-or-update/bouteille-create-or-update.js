class BouteillesCreateOrUpdate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {types: [], regions: [], volumes: [], cepages: [], casiers: [], appellations: [], form: {etoile: 1, type: 'Rouge', stock: 1, compositions: [], emplacements: [], est_indicee: true}};

        this.setValue = (val, evt) => {
            this.state.form[evt.target.name] = val;
            return new Promise((resolve) => this.forceUpdate(resolve));
        };

        this.addCepageForm = (evt) => {
            evt.preventDefault();
            evt.stopPropagation();
            var percent = 100;
            for (let composition of this.state.form.compositions) {
                percent -= composition.pourcentage;
                if (percent < 10) {
                    percent = 10;
                }
            }
            this.state.form.compositions.push({pourcentage: percent, cepage: ""});
            this.forceUpdate();
        };

        this.addEmplacementForm = (evt)=> {
            evt.preventDefault();
            evt.stopPropagation();
            this.state.form.emplacements.push({casier: this.state.form.emplacements.length && this.state.form.emplacements[this.state.form.emplacements.length - 1].casier || this.state.casiers[0][0]});
            this.state.form.stock = this.state.form.emplacements.length;
            this.forceUpdate();
        };

        this.removeEmplacementForm = (evt, i)=> {
            evt.preventDefault();
            evt.stopPropagation();
            this.state.form.emplacements.splice(i, 1);
            this.state.form.stock = this.state.form.emplacements.length;
            this.forceUpdate();
        };

        this.saveOrUpdate = (evt) => {
            evt.preventDefault();
            evt.stopPropagation();
            this.setState({errorText: null});

            this.state.form.volumeId = this.state.form.volume.id;

            this.submit(this.state.form)
                .then((data) => new Promise(resolve => {
                    window.location.hash = "#/bouteilles/" + data.id;
                    resolve(data);
                }))
                .catch((data) => new Promise(resolve => {
                    this.setState({errorText: data.responseText});
                    resolve();
                }))
        };

        this.setRegion = (value, event) => {
            this.setValue(value, event)
                .then(() => new Promise((resolve) => {
                    var ctx = this;
                    this.updateAppellations().then(() => {
                        ctx.forceUpdate();
                    });
                }));
        };

        this.updateTypes = () => Ajax.get("/api/types").then(types => new Promise(resolve=> {
            types.sort((a, b) => sortObj(a, b, "nom")).sort((a, b) => sortObj(a, b, "ordre")).map((v)=> {
                this.state.form.type = this.state.form.type || v.nom;
                this.state.types.push([v.nom, v.nom]);
            });
            resolve();
        }));

        this.updateRegions = () => Ajax.get("/api/regions").then(regions => new Promise(resolve => {
            regions.sort((a, b) => sortObj(a, b, "nom")).sort((a, b) => sortObj(a, b, "ordre")).map((v)=> {
                this.state.form.region = this.state.form.region || v.nom;
                this.state.regions.push([v.nom, v.nom]);
            });
            resolve();
        }));

        this.updateAppellations = () => {
            return Ajax.get("/api/regions/" + encodeURIComponent(this.state.form.region) + "/appellations")
                .then(appellations => new Promise(resolve => {
                    this.state.appellations = [];
                    appellations.sort((a, b) => sortObj(a, b, "nom")).map((v)=> {
                        this.state.appellations.push(v.nom);
                    });
                    resolve();
                }));
        };

        this.updateBouteille = (id) => Ajax.get("/api/bouteilles/" + id).then(bouteille => new Promise(resolve => {
            this.state.form = Object.assign({}, bouteille);

            this.state.form.type = bouteille.type.nom;
            this.state.form.region = bouteille.region.nom;
            this.state.form.appellation = bouteille.appellation && bouteille.appellation.nom;

            this.state.form.compositions = [];
            bouteille.compositions.sort((a, b) => sortObj(a, b, "pourcentage", 'desc')).map((v) => {
                this.state.form.compositions.push({pourcentage: v.pourcentage, cepage: v.cepage.nom});
            });

            this.state.form.emplacements = [];
            bouteille.emplacements.sort((a, b) => sortObj(a, b, "casier.nom", 'asc')).map((v) => {
                this.state.form.emplacements.push({casier: v.casier.nom, colonne: v.colonne, ligne: v.ligne});
            });

            resolve();
        }));

        this.updateCasiers = () => Ajax.get("/api/casiers").then(casiers => new Promise(resolve => {
            casiers.sort((a, b) => sortObj(a, b, "nom")).map((v)=> {
                this.state.casiers.push([v.nom, v.nom]);
                if (!this.state.form.casier) {
                    this.state.form.casier = v.nom;
                }
            });
            resolve();
        }));

        this.updateVolumes = () => Ajax.get("/api/volumes").then(volumes => new Promise(resolve => {
            volumes.sort((a, b) => sortObj(a, b, "volume")).map((v)=> {
                this.state.volumes.push([v, v.nom]);
                if (!this.state.form.volume && 750 === v.volume) {
                    this.state.form.volume = v;
                }
            });
            resolve();
        }));

        this.updateCepages = () => Ajax.get("/api/cepages").then(cepages => new Promise(resolve => {
            cepages.sort((a, b) => sortObj(a, b, "nom")).map((v)=> {
                this.state.cepages.push(v.nom);
            });
            resolve();
        }));

        new Promise(resolve => resolve())
            .then(()=> ('undefined' !== typeof(this.props.id)) ? this.updateBouteille(this.props.id) : new Promise(resolve => resolve()))
            .then(this.updateTypes)
            .then(this.updateRegions)
            .then(this.updateAppellations)
            .then(this.updateCasiers)
            .then(this.updateVolumes)
            .then(this.updateCepages)
            .then(() => new Promise(resolve => this.forceUpdate(resolve)))
            .catch(e => console.log(e));
    }

    render() {
        return (
            <form autoComplete="off" onSubmit={(evt) => this.saveOrUpdate(evt)}>
                <h1><I18n>{this.title}</I18n></h1>
                <InputTextSingle id="nom" label="nom" placeholder="nom" onChange={this.setValue} validator={Validators.string({min: 3, required: true})} value={this.state.form.nom}/>
                {this.state.form.compositions.map((o, i)=> {
                    return (
                        <FormGroup key={i}>
                            <FormLabel width="2" id={"compositionPourcentage"+i} label={i === 0 ? "composition" : ""}/>
                            <Column width="2">
                                <InputGroup>
                                    <InputSelect id={"compositionPourcentage"+i} placeholder="%" onChange={(value)=> {this.state.form.compositions[i].pourcentage = parseInt(value); this.forceUpdate()}} value={this.state.form.compositions[i].pourcentage} values={[[10, 10], [20, 20], [30, 30], [40, 40], [50, 50], [60, 60], [70, 70], [80, 80], [90, 90], [100, 100]]}/>
                                    <InputGroupAppend label="%"/>
                                </InputGroup>
                            </Column>
                            <Column width="7">
                                <InputText id={"compositionCepage"+i} placeholder="cepage" onChange={(value)=> {this.state.form.compositions[i].cepage = value; this.forceUpdate()}} values={this.state.cepages} value={this.state.form.compositions[i].cepage}/>
                            </Column>
                            <Column width="1">
                                <button onClick={(evt)=> {this.state.form.compositions.splice(i, 1); this.forceUpdate(); evt.preventDefault(); evt.stopPropagation();}} className="btn btn-link text-danger"><i className="fa fa-remove"></i></button>
                            </Column>
                        </FormGroup>)
                })}
                <FormGroup>
                    <Column width="2">
                    </Column>
                    <Column width="10">
                        <button onClick={this.addCepageForm} className="btn btn-link text-success"><i className="fa fa-plus"></i> Ajouter un cépage</button>
                    </Column>
                </FormGroup>

                <InputSelectSingle id="type" label="type" onChange={this.setValue} values={this.state.types} value={this.state.form.type}/>
                <InputSelectSingle id="region" label="region" onChange={this.setRegion} values={this.state.regions} value={this.state.form.region}/>
                <InputTextSingle id="appellation" label="appellation" placeholder="appellation" onChange={this.setValue} values={this.state.appellations} value={this.state.form.appellation || ''}/>
                <InputTextSingle type="numeric" id="millesime" label="millesime" placeholder="0000" onChange={this.setValue} value={this.state.form.millesime} validator={Validators.number({min: 1900, max: new Date().getFullYear(), required: true})}/>
                <InputTextSingle type="numeric" id="boire_apres" label="a consommer apres" placeholder="0000" onChange={this.setValue} value={this.state.form.boire_apres}/>
                <InputTextSingle type="numeric" id="boire_apogee" label="apogee" placeholder="0000" onChange={this.setValue} value={this.state.form.boire_apogee}/>
                <InputTextSingle type="numeric" id="boire_avant" label="a consommer avant" placeholder="0000" onChange={this.setValue} value={this.state.form.boire_avant} validator={Validators.number({min: 1900})}/>
                <InputSelectSingle id="volume" label="Volume" onChange={this.setValue} values={this.state.volumes} value={this.state.form.volume}/>
                <InputTextSingle type="numeric" id="stock" label="stock" placeholder="0" onChange={this.setValue} value={this.state.form.stock} validator={Validators.number({min: 0, required: true})}/>

                {this.state.form.emplacements.map((o, i)=> {
                    return (
                        <FormGroup key={i}>
                            <FormLabel width="2" id={"emplacementCasier"+i} label={i === 0 ? "Emplacement" : ""}/>
                            <InputGroup width="5">
                                <InputGroupPrepend label="casier"/>
                                <InputSelect width="12" id={"emplacementCasier"+i} placeholder="%" onChange={(value)=> {this.state.form.emplacements[i].casier = value; this.forceUpdate()}} value={this.state.form.emplacements[i].casier} values={this.state.casiers}/>
                            </InputGroup>
                            <InputGroup width="2">
                                <InputGroupPrepend label="colonne"/>
                                <InputText width="12" type="numeric" id={"emplacementColonne"+i} placeholder="1-99" onChange={(value)=> {this.state.form.emplacements[i].colonne = parseInt(value); this.forceUpdate()}} value={this.state.form.emplacements[i].colonne}/>
                            </InputGroup>
                            <InputGroup width="2">
                                <InputGroupPrepend label="ligne"/>
                                <InputText width="12" type="text" id={"emplacementligne"+i} placeholder="A-Z" onChange={(value)=> {this.state.form.emplacements[i].ligne = StringUtils.unRomanize(value); this.forceUpdate()}} value={StringUtils.romanize(this.state.form.emplacements[i].ligne)}/>
                            </InputGroup>
                            <Column width="1">
                                <button onClick={(evt) => this.removeEmplacementForm(evt, i)} className="btn btn-link text-danger"><i className="fa fa-remove"></i></button>
                            </Column>
                        </FormGroup>)
                })}
                <FormGroup>
                    <Column width="2">
                    </Column>
                    <Column width="10">
                        <button onClick={this.addEmplacementForm} className="btn btn-link text-success"><i className="fa fa-plus"></i> Ajouter un emplacement</button>
                    </Column>
                </FormGroup>
                <InputTextSingle id="prix" label="prix" placeholder="00.00" onChange={this.setValue} value={this.state.form.prix} validator={Validators.number({min: 1})}/>
                <InputCheckboxSingle width="1" label="indicer" id="est_indicee" value={this.state.form.est_indicee} onChange={this.setValue}/>
                <FormGroup>
                    <FormLabel width="2" label="note"/>
                    <Column width="5">
                        <Etoile value={this.state.form.etoile} onChange={(value) => {this.state.form.etoile = value; this.forceUpdate();}}/>
                    </Column>
                </FormGroup>
                <InputTextSingle id="description" label="description" placeholder="description" onChange={this.setValue} value={this.state.form.description}/>
                {this.state.errorText
                    ? <div className="alert alert-danger" id="submitError">{this.state.errorText}</div>
                    : <div className="alert">&nbsp;</div>
                }
                <InputButton className="primary">{this.submitText}</InputButton>
            </form>
        );
    }
}
