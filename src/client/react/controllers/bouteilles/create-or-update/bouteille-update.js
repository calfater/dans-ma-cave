class BouteillesUpdate extends BouteillesCreateOrUpdate {
    constructor(props) {
        super(props);
        this.title = "mettre a jour titre";
        this.submitText = "mettre a jour";

        this.submit = function (data) {
            return Ajax.put("/api/bouteilles/" + data.id, data);
        };

    }
}
