class BouteillesListeFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                prixInfA: Url.getHashParam('prixInfA')
            },
            regions: []
        };

        Ajax.get("/api/regions").then(data => {
                this.state.regions = data;
            })
            .then(() => this.forceUpdate());
    }

    render() {
        return (
            <div className="collapse form-filter" id="bouteillesListeFilter">
                <form className="form">
                    <Row>
                        <Column width="12">
                            <FormGroup>
                                <FormLabel width="1" label="Prix" id="prixInfA"/>
                                <InputText width="1" id="prixSupA" onChange={(value) => {this.state.form.prixSupA = value; Url.addHashParam("prixSupA", value); this.forceUpdate();}} value={this.state.form.prixSupA}/>
                                <FormLabel width="1" label="->" id="prixSupA"/>
                                <InputText width="1" id="prixInfA" onChange={(value) => {this.state.form.prixInfA= value; Url.addHashParam("prixInfA", value); this.forceUpdate();}} value={this.state.form.prixInfA}/>
                            </FormGroup>
                        </Column>
                    </Row>
                    <Row>
                        <FormLabel width="1" label="Région" id="region"/>
                        <InputSelect width="4" id="region" values={this.state.regions}/>
                    </Row>
                </form>
            </div>
        );
    }
}
