class BouteillesListe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {bouteilles: [], bouteilleCache: [], orderBy: '', way: 'asc'};

        this.updateSortParams = (orderBy) => {
            var way = 'asc';
            if (this.state.orderBy === orderBy) {
                way = ('asc' === this.state.way ? 'desc' : 'asc')
            }
            this.setState({way: way, orderBy: orderBy});
        };

        this.filter = () => {
            var filteredData = [];
            this.state.bouteilleCache.map(bouteille => {
                if ("true" !== Url.getHashParam('stockVide') && 0 >= bouteille.stock) {
                    return;
                }
                if ("true" === Url.getHashParam('stockVide') && 0 < bouteille.stock) {
                    return;
                }
                if (Url.getHashParam('region') && bouteille.region.nom !== Url.getHashParam('region')) {
                    return;
                }
                if (Url.getHashParam('type') && bouteille.type.nom !== Url.getHashParam('type')) {
                    return;
                }
                if (Url.getHashParam('volume') && bouteille.volume.nom !== Url.getHashParam('volume')) {
                    return;
                }
                if (Url.getHashParam('etoile') && bouteille.etoile !== parseInt(Url.getHashParam('etoile'))) {
                    return;
                }

                var apogee = Url.getHashParam('apogee');
                if (undefined === apogee) {
                    apogee = undefined;
                } else if ("null" === apogee) {
                    apogee = null;
                } else {
                    apogee = parseInt(Url.getHashParam('apogee'));
                }
                if (undefined !== apogee && bouteille.boire_apogee !== apogee) {
                    return;
                }

                if (('undefined' != typeof(Url.getHashParam('prixInfA')) && '' !== Url.getHashParam('prixInfA')) && (null === bouteille.prix || bouteille.prix > parseInt(Url.getHashParam('prixInfA')))) {
                    return;
                }
                if (('undefined' != typeof(Url.getHashParam('prixSupA')) && '' != Url.getHashParam('prixSupA')) && (null === bouteille.prix || bouteille.prix < parseInt(Url.getHashParam('prixSupA')))) {
                    return;
                }
                filteredData.push(bouteille);
            });
            this.state.bouteilles = filteredData;
        };

        this.sort = () => {
            this.state.bouteilles.sort((a, b) => sortObj(a, b, this.state.orderBy, this.state.way));
        };

        Ajax.get("/api/bouteilles").then(data => {
            Ajax.get("/api/configs").then(config => {
                let enjoliveNom = config.enjolive_nom;
                this.state.bouteilleCache = [];
                data.map(bouteille => {
                    if ("true" === enjoliveNom) {
                        this.state.bouteilleCache.push(Object.assign(bouteille, {nom: StringUtils.enjoliveLeNom(bouteille.nom)}));
                    } else {
                        this.state.bouteilleCache.push(bouteille);
                    }
                });
                this.filter();
                this.updateSortParams('region.nom');
                this.state.colonnesAAfficher = config.colonnes_a_afficher.split(',');
                this.forceUpdate();
            });
        });
    }

    render() {
        this.filter();
        this.sort();

        return (
            <Row className="bouteilles-liste">
                <Column>
                    <BouteillesListeFilter />
                    <Table>
                        <TableHead>
                            <TableRow>
                                {this.state.colonnesAAfficher && this.state.colonnesAAfficher.map(nom => {
                                    switch (nom.trim()) {
                                        case "nom":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="nom" by="nom"/>);
                                        case "type":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="type" by="type.nom"/>);
                                        case "region":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="region" by="region.nom"/>);
                                        case "appellation":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="appellation" by="appellation.nom"/>);
                                        case "millesime":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="millesime" by="millesime" type="numeric"/>);
                                        case "boire_apres":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="apres" by="boire_apres" type="numeric"/>);
                                        case "boire_apogee":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="apogee" by="boire_apogee" type="numeric"/>);
                                        case "boire_avant":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="avant" by="boire_avant" type="numeric"/>);
                                        case "volume":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="volume" by="volume.volume" type="numeric"/>);
                                        case "quantite":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="quantite" by="stock" type="numeric"/>);
                                        case "prix":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="prix" by="prix" type="numeric"/>);
                                        case "indice":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="indice" by="indice.valeur" type="numeric"/>);
                                        case "etoile":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="note" by="etoile" type="numeric"/>);
                                        case "cepages":
                                            return (<ThSortable key={nom} sort={this.updateSortParams} orderBy={this.state.orderBy} way={this.state.way} label="cepages" by=""/>);
                                    }
                                })}
                                {/*
                                 */}
                            </TableRow>
                        </TableHead>
                        <TableBody className="table-striped">
                            {
                                this.state.bouteilles.map((bouteille) => {
                                    return <BouteillesListeLine key={bouteille.id} display={this.state.colonnesAAfficher} bouteille={bouteille}/>;
                                })
                            }
                        </TableBody>
                    </Table>
                </Column>
            </Row>
        );
    }
}
