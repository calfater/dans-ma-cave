class BouteillesListeLine extends React.Component {
    render() {
        return (
            <TableRow className="bouteille-table-line">


                {this.props.display && this.props.display.map(nom => {
                    switch (nom.trim()) {
                        case "nom":
                            return (
                                <TableColumn key={nom} className="nom">
                                    <a href={"#/bouteilles/" + this.props.bouteille.id}>{this.props.bouteille.nom}</a>
                                    {this.props.bouteille.est_indicee ? <ProgressBar width={this.props.bouteille.indice.valeur_pourcent}/> : ""}
                                </TableColumn>
                            );
                        case "type":
                            return (<TableColumn key={nom} className="filter" onClick={() => Url.addHashParam('type', this.props.bouteille.type.nom)}>{this.props.bouteille.type.nom}</TableColumn>);
                        case "region":
                            return (<TableColumn key={nom} className="filter" onClick={() => Url.addHashParam('region', this.props.bouteille.region.nom)}>{this.props.bouteille.region.nom}</TableColumn>);
                        case "appellation":
                            return (<TableColumn key={nom}>{this.props.bouteille.appellation ? this.props.bouteille.appellation.nom : ""}</TableColumn>);
                        case "millesime":
                            return (
                                <TableColumn key={nom}>
                                    <span className="d-none d-lg-block">{this.props.bouteille.millesime}</span>
                                    <span className="d-lg-none">{this.props.bouteille.millesime && ("" + this.props.bouteille.millesime).substr(2)}</span>
                                </TableColumn>
                            );
                        case "boire_apres":
                            return (
                                <TableColumn key={nom}>
                                    <span className="d-none d-lg-block">{this.props.bouteille.boire_apres}</span>
                                    <span className="d-lg-none">{this.props.bouteille.boire_apres && ("" + this.props.bouteille.boire_apres).substr(2)}</span>
                                </TableColumn>
                            );
                        case "boire_apogee":
                            return (
                                <TableColumn key={nom}>
                                    <span className="d-none d-lg-block">{this.props.bouteille.boire_apogee}</span>
                                    <span className="d-lg-none">{this.props.bouteille.boire_apogee && ("" + this.props.bouteille.boire_apogee).substr(2)}</span>
                                </TableColumn>

                            );
                        case "boire_avant":
                            return (
                                <TableColumn key={nom}>
                                    <span className="d-none d-lg-block">{this.props.bouteille.boire_avant}</span>
                                    <span className="d-lg-none">{this.props.bouteille.boire_avant && ("" + this.props.bouteille.boire_avant).substr(2)}</span>
                                </TableColumn>
                            );
                        case "volume":
                            return (<TableColumn key={nom} className="filter" onClick={() => Url.addHashParam('volume', this.props.bouteille.volume.nom)}>{this.props.bouteille.volume.volume / 1000 + 'L'}</TableColumn>);
                        case "quantite":
                            return (<TableColumn key={nom}>{this.props.bouteille.stock && this.props.bouteille.stock}</TableColumn>);
                        case "prix":
                            return (<TableColumn key={nom}>{this.props.bouteille.prix && this.props.bouteille.prix + '€'}</TableColumn>);
                        case "indice":
                        {
                            return (this.props.bouteille.est_indicee ?
                                <TableColumn key={nom} className="indice" title={
                                        "Maturation : + " + this.props.bouteille.indice.detail.maturation + "\n" +
                                        "Prix : + " + this.props.bouteille.indice.detail.prix + "\n" +
                                        "Etoile : + " + this.props.bouteille.indice.detail.etoile + "\n" +
                                        "Volume : x " + this.props.bouteille.indice.detail.volume + "\n"
                                    }>
                                    <ProgressBar width={this.props.bouteille.indice.valeur_pourcent}>{this.props.bouteille.indice.valeur}</ProgressBar>
                                </TableColumn>
                                :
                                <TableColumn key={nom}/>)
                        }

                        case "etoile":
                            return (<TableColumn key={nom} className="filter" onClick={() => Url.addHashParam('etoile', this.props.bouteille.etoile)}><Etoile value={this.props.bouteille.etoile}/></TableColumn>);
                        case "cepages":
                            return (
                                <TableColumn key={nom}>
                                    <ul className="compositionList">
                                        {this.props.bouteille.compositions.sort((a, b) => sortObj(a, b, "pourcentage", 'desc')).map((composition, idx) => {
                                            return (<li key={idx}>{composition.cepage.nom + (100 != composition.pourcentage ? " (" + composition.pourcentage + "%)" : "")}</li>)
                                        }) }
                                    </ul>
                                </TableColumn>
                            );
                    }
                })}
                {/*
                 */}


            </TableRow>
        );
    }
}
