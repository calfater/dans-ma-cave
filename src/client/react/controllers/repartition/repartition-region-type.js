class RepartitionRegionType extends React.Component {

    constructor(props) {
        super(props);
        this.state = {data: {}};
    }

    render() {
        this.state.data = this.props.data || {};

        return (
            <div className="wrapper">
                <h2><I18n>repartition par region et type</I18n></h2>
                <Row>
                    <Column width="12">
                        <Table className="table-striped table-bordered">
                            <TableHead>
                                <TableRow>
                                    <TableHeadColumn></TableHeadColumn>
                                    {
                                        Object.entries(this.state.data).length
                                            ? Object.entries(Object.entries(this.state.data)[0][1]).map((v) => {
                                            return (<TableHeadColumn key={v[0]} className="filter" onClick={()=> Url.goTo("/bouteilles", {type: v[0]})}>{v[0]}</TableHeadColumn>);
                                        })
                                            : null
                                    }
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    Object.entries(this.state.data).map((regionEntry) => {
                                        var entryRegionNom = regionEntry[0];
                                        var entryRegionType = regionEntry[1];
                                        return (
                                            <TableRow key={entryRegionNom}>
                                                <TableColumn className="filter" onClick={()=> Url.goTo("/bouteilles", {region: regionEntry[0]})}>{regionEntry[0]}</TableColumn>
                                                {
                                                    Object.entries(entryRegionType).map((entryType) => {
                                                        var entryTypeNom = entryType[0];
                                                        var entryQuantite = entryType[1];
                                                        return (
                                                            <td key={entryRegionNom + "-" + entryTypeNom} className="filter" onClick={()=> Url.goTo("/bouteilles", {region: entryRegionNom, type: entryTypeNom})}>
                                                                {entryQuantite.quantiteTotale ? (entryQuantite.quantiteTotale + " (~" + entryQuantite.quantiteDifferente + ")") : ""}
                                                            </td>
                                                        )
                                                    })
                                                }
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </Column>
                </Row>
            </div>
        );
    }
}
