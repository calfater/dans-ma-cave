class RepartitionApogee extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.sort = (by, way) => {
            this.state.data = this.state.data.sort((a, b) => sortObj(a, b, by, way));
        };

        this.onClick = (x) => {
            Url.goTo("/bouteilles", {apogee: x});
        };
    }

    render() {
        if (!this.state.data && this.props.data) {
            this.state.data = [];
            this.props.data.map((a) => {
                    if (a.apogee) {
                        this.state.data.push({label: a.apogee, value: a.quantite});
                    }
                }
            );
            this.sort('label');
        }

        return (
            <div className="wrapper">
                <h2><I18n>repartition par annee d apogee</I18n></h2>
                <Row>
                    <Column width="4">
                        <Table2Cols data={this.state.data} onSort={(by, way) => {this.sort(by,way);this.forceUpdate()}} label="apogee" onClick={this.onClick} defaultSort="label"/>
                    </Column>
                    <Column width="8">
                        <Chart_ id="repartitionParApogee" data={this.state.data} onClick={this.onClick}/>
                        <InputButton className="light" onClick={()=> {this.onClick("null")}}>sans apogee</InputButton>
                    </Column>
                </Row>
            </div>
        );
    }
}
