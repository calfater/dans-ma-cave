class RepartitionRegion extends React.Component {

    constructor(props) {
        super(props);

        this.state = {};

        this.sort = (by, way) => {
            var data = this.state.data.sort((a, b) => sortObj(a, b, by, way));
            if (by === 'label') {
                data = this.state.data.sort((a, b) => sortObj(a, b, "ordre", 'asc'));
            }
            this.state.data = data;
        }

        this.onClick = (x) => {
            Url.goTo("/bouteilles", {region: x});
        };

    }

    render() {
        if (!this.state.data && this.props.data) {
            this.state.data = [];
            this.props.data.map((a) => {
                    this.state.data.push({ordre: a.ordre, label: a.nom, value: a.quantite});
                }
            );
            this.sort('value', 'desc');
        }

        return (
            <div className="wrapper">
                <h2><I18n>repartition par region</I18n></h2>
                <Row>
                    <Column width="4">
                        <Table2Cols data={this.state.data} onSort={(by, way) => {this.sort(by,way);this.forceUpdate()}} label="region" onClick={this.onClick}/>
                    </Column>
                    <Column width="8">
                        <Chart_ id="repartitionParRegion" data={this.state.data} onClick={this.onClick}/>
                    </Column>
                </Row>
            </div>
        );
    }
}
