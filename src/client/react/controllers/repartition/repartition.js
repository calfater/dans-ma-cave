class Repartition extends React.Component {
    constructor(props) {
        super(props);
        this.state = {repartition: {}};

        Ajax.get("/api/repartition").then(data => new Promise(resolve => {
            this.setState({repartition: data}, resolve);
        }));
    }

    render() {

        return (
            <div className="repartition">
                <RepartitionApogee data={this.state.repartition.apogee}/>
                <RepartitionRegion data={this.state.repartition.region}/>
                <RepartitionType data={this.state.repartition.type}/>
                <RepartitionRegionType data={this.state.repartition.regionEtType}/>
            </div>
        );
    }
}
