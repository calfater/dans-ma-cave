class Menu extends React.Component {

    componentDidMount() {
        var navMain = $(".navbar-collapse");
        navMain.on("click", "a:not([data-toggle])", null, function () {
            navMain.collapse('hide');
        });
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand" href="#"><I18n>Dans ma cave</I18n></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className={"nav-item" + (this.props.page !== "bouteilles-ajouter" && this.props.page.startsWith("bouteilles") || "" === this.props.page ? " active" : "")}>
                            <a className="nav-link" href="#/bouteilles"><i className="fa fa-list"></i> <I18n>menu lister</I18n></a>
                        </li>
                        <li className={"nav-item" + (this.props.page === "bouteilles-ajouter" ? " active" : "")}>
                            <a className="nav-link" href="#/bouteilles/ajouter"><i className="fa fa-plus"></i> <I18n>menu ajouter</I18n></a>
                        </li>
                        <li className={"nav-item" + (this.props.page === "repartition" ? " active" : "")}>
                            <a className="nav-link" href="#/repartition"><i className="fa fa-bar-chart"></i> <I18n>menu repartition</I18n></a>
                        </li>
                        <li className={"nav-item" + (this.props.page === "emplacements" ? " active" : "")}>
                            <a className="nav-link" href="#/emplacements"><i className="fa fa-building"></i> <I18n>menu casiers</I18n></a>
                        </li>
                    </ul>
                    <ul className="navbar-nav ml-auto">
                        {this.props.page === "bouteilles" || this.props.page === "" ?
                            <li className={"nav-item" + (this.props.page === "emplacements" ? " active" : "")}>
                                <button className="nav-link btn btn-link" type="button" data-toggle="collapse" data-target="#bouteillesListeFilter" aria-expanded="false" aria-controls="collapseExample"><i className="fa fa-search"></i> <I18n>menu filtrer</I18n></button>
                            </li>
                            : ""
                        }
                        <li className="nav-item dropdown">
                            <a className="nav-link" href="#" data-toggle="dropdown"><i className="fa fa-gear"></i> <I18n>menu admin</I18n></a>
                            <ul className="dropdown-menu dropdown-menu-right bg-dark">
                                <li className="dropdown-item bg-dark"><a className="nav-link" href="#/admin/mdp"><i className="fa fa-user"></i> <I18n>menu admin identifiants</I18n></a></li>
                                <li className="dropdown-item bg-dark"><a className="nav-link" href="#/admin/casiers"><i className="fa fa-building"></i> <I18n>menu admin casiers</I18n></a></li>
                                <li className="dropdown-item bg-dark"><a className="nav-link" href="#/admin/parametres"><i className="fa fa-dashboard"></i> <I18n>menu admin parametres</I18n></a></li>
                                <li className="dropdown-item bg-dark"><a className="nav-link" href="#/bouteilles?stockVide=true"><i className="fa fa-eye"></i> <I18n>menu admin afficher les stocks vides</I18n></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}
