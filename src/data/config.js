module.exports = {
    db: {
        host: 'localhost',
        username: 'root',
        password: 'secret',
        port: 3306,
        database: 'test',
        storage: __dirname + '/dmc.sqlite',
        //logs: console.log,
         logs: false,
        //dialect: 'mysql'
        dialect: 'sqlite'
    }
};
