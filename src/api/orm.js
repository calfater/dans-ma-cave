const Sequelize = require('sequelize');
const fs = require('fs');

const config = require("./../data/config");
const logger = require('./utils/logger');

var sequelizeInstance = null;

module.exports = {
    get: () => {
        if (null === sequelizeInstance) {
            console.log("Create Sequelize instance");
            sequelizeInstance = new Sequelize({
                database: config.db.database,
                host: config.db.host,
                username: config.db.username,
                password: config.db.password,
                port: config.db.port,
                dialect: config.db.dialect,
                storage: config.db.storage,
                operatorsAliases: false,
                dialectOptions: {
                    multipleStatements: true
                },
                define: {
                    timestamps: false,
                    underscored: true
                },
                logging: config.db.logs || false
            });
        }
        return sequelizeInstance;
    },
    makeRel: () => {
        Bouteille = require('./entities/bouteille');
        Type = require('./entities/type');
        Region = require('./entities/region');
        Appellation = require('./entities/appellation');
        Volume = require('./entities/volume');
        Casier = require('./entities/casier');
        Cepage = require('./entities/cepage');
        Composition = require('./entities/composition');
        Emplacement = require('./entities/emplacement');

        Bouteille.belongsTo(Type);
        Bouteille.belongsTo(Region);
        Bouteille.belongsTo(Appellation);
        Bouteille.belongsTo(Volume);
        Bouteille.hasMany(Composition, {onDelete: 'cascade', hooks: true});
        Bouteille.hasMany(Emplacement, {onDelete: 'cascade', hooks: true});

        Type.hasMany(Bouteille);

        Region.hasMany(Bouteille);
        Region.hasMany(Appellation);

        Appellation.belongsTo(Region);

        Appellation.hasMany(Bouteille);

        Casier.hasMany(Emplacement);

        Volume.hasMany(Bouteille);

        Composition.belongsTo(Bouteille);
        Composition.belongsTo(Cepage);

        Emplacement.belongsTo(Bouteille);
        Emplacement.belongsTo(Casier);

        Cepage.hasMany(Composition);
        return new Promise(resolve => resolve());
    },

    Sequelize: Sequelize,

    updateSchema: async () => {
        const configService = require('./services/config-service');

        var dbVersion = parseInt(await configService.get('db_version').catch(()=>new Promise(resolve => resolve(-1))) || -1);
        var sqlPath = __dirname + '/../resources/db/';
        var files = [];

        console.log("Updating schema from " + dbVersion);

        fs.readdirSync(sqlPath).forEach(file => {
            files.push(file);
        });

        files.sort();

        var fileIdx = 0;
        var query = sequelizeInstance.query("PRAGMA foreign_keys = OFF");
        for (; fileIdx < files.length; fileIdx++) {
            //console.log(fileIdx, dbVersion, files[fileIdx]);
            if (fileIdx > dbVersion) {
                var file = files[fileIdx];
                console.log(" - " + file.replace(/\d* /, "").replace(".sql", ""));
                var sql = fs.readFileSync(sqlPath + file, 'utf8');
                for (var s of sql.replace(/;$/, '').split(";")) {
                    if ("" !== s.trim()) {
                        if ('sqlite' === config.db.dialect) {
                            s = s.replace("\\'", "''");
                            s = s.replace('NOT NULL AUTO_INCREMENT', 'AUTOINCREMENT');
                            s = s.replace('AUTO_INCREMENT', 'AUTOINCREMENT');
                            //s = "PRAGMA foreign_keys = ON; " + s;
                        }
                        query = query.then(()=> sequelizeInstance.query(s));
                        await query;
                    }
                }
            }
        }
        await query.then(()=> configService.set({db_version: fileIdx - 1})).catch(e => console.log(e));
        console.log("Update schema done.");

        return new Promise(resolve => resolve());
    }

};
