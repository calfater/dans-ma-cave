const orm = require("./../orm");

var Casier = orm.get().define('casier', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    nom: {type: orm.Sequelize.STRING},
    nombre_de_colonnes: {type: orm.Sequelize.INTEGER},
    nombre_de_lignes: {type: orm.Sequelize.INTEGER}
});

module.exports = Casier;

