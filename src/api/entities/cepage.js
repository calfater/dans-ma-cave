const orm = require("./../orm");

var Cepage = orm.get().define('cepage', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    nom: {type: orm.Sequelize.STRING}
});

module.exports = Cepage;

