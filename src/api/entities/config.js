const orm = require("./../orm");

var Config = orm.get().define('config', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    cle: {type: orm.Sequelize.STRING},
    valeur: {type: orm.Sequelize.STRING}
});

module.exports = Config;
