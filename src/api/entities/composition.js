const orm = require("./../orm");
const Bouteille = require('./bouteille');
const Cepage = require('./cepage');

var Composition = orm.get().define('composition', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    bouteille_id: {type: orm.Sequelize.INTEGER, references: {model: Bouteille, key: 'id'}},
    cepage_id: {type: orm.Sequelize.INTEGER, references: {model: Cepage, key: 'id'}},
    pourcentage: {type: orm.Sequelize.INTEGER}
});

module.exports = Composition;

