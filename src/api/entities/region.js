const orm = require("./../orm");

var Region = orm.get().define('region', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    nom: {type: orm.Sequelize.STRING},
    ordre: {type: orm.Sequelize.INTEGER}
});

module.exports = Region;

