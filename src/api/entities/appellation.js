const orm = require("./../orm");
const Region = require('./region');

var Appellation = orm.get().define('appellation', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    region_id: {type: orm.Sequelize.INTEGER, references: {model: Region, key: 'id'}},
    nom: {type: orm.Sequelize.STRING}
});

module.exports = Appellation;

