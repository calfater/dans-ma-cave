const orm = require("./../orm");
const Type = require("./type");
const Region = require("./region");
const Appellation = require("./appellation");
const Volume = require("./volume");
const Casier = require("./casier");

var Bouteille = orm.get().define('bouteille', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    nom: {type: orm.Sequelize.STRING},
    type_id: {type: orm.Sequelize.INTEGER, references: {model: Type, key: 'id'}},
    region_id: {type: orm.Sequelize.INTEGER, references: {model: Region, key: 'id'}},
    appellation_id: {type: orm.Sequelize.INTEGER, references: {model: Appellation, key: 'id'}},
    millesime: {type: orm.Sequelize.INTEGER},
    boire_apres: {type: orm.Sequelize.INTEGER},
    boire_apogee: {type: orm.Sequelize.INTEGER},
    boire_avant: {type: orm.Sequelize.INTEGER},
    volume_id: {type: orm.Sequelize.INTEGER, references: {model: Volume, key: 'id'}},
    prix: {type: orm.Sequelize.DECIMAL},
    stock: {type: orm.Sequelize.INTEGER},
    est_indicee: {type: orm.Sequelize.BOOLEAN},
    etoile: {type: orm.Sequelize.INTEGER},
    description: {type: orm.Sequelize.TEXT}
});

module.exports = Bouteille;
