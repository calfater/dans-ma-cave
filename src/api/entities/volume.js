const orm = require("./../orm");

var Volume = orm.get().define('volume', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    volume: {type: orm.Sequelize.INTEGER},
    nom: {type: orm.Sequelize.STRING}
});

module.exports = Volume;
