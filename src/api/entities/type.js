const orm = require("./../orm");

var Type = orm.get().define('type', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    nom: {type: orm.Sequelize.STRING},
    ordre: {type: orm.Sequelize.INTEGER}
});

module.exports = Type;

