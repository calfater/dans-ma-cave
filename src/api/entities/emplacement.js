const orm = require("./../orm");
const Bouteille = require('./bouteille');
const Casier = require('./casier');

var Emplacement = orm.get().define('emplacement', {
    id: {type: orm.Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    bouteille_id: {type: orm.Sequelize.INTEGER, references: {model: Bouteille, key: 'id'}},
    casier_id: {type: orm.Sequelize.INTEGER, references: {model: Casier, key: 'id'}},
    colonne: {type: orm.Sequelize.INTEGER},
    ligne: {type: orm.Sequelize.INTEGER}
});

module.exports = Emplacement;
