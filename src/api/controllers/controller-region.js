const regionService = require('./../../api/services/region-service');
const appellationService = require('./../../api/services/appellation-service');

var init = (app) => {
    app.register('get', '/api/regions', 'Liste toutes les régions', function (req, res) {
        res.contentType("application/json");
        return regionService.findAll()
            .then((regions) => {
                res.status(200);
                res.write(JSON.stringify(regions));
                res.end()
            })
    });

    app.register('get', '/api/regions/:regionIdOrNom/appellations', 'Liste touts les appellations dans une région', function (req, res) {
        res.contentType("application/json");
        return appellationService.getByRegionIdOrNom(req.params.regionIdOrNom, req.params.regionIdOrNom)
            .then(appellations => new Promise(resolve => {
                res.status(200);
                res.write(JSON.stringify(appellations));
                resolve();
                res.end()
            }))
    });


};

module.exports = init;
