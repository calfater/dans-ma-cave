const casierService = require('./../../api/services/casier-service');

var init = (app) => {
    app.register('get', '/api/casiers', 'Liste l\'ensemble des casiers', (req, res) => {
        res.contentType("application/json");
        return casierService.findAll()
            .then((casiers) => new Promise((resolve)=> {
                    res.status(200);
                    res.write(JSON.stringify(casiers));
                    res.end();
                    resolve();
                })
            )
    });

    app.register('post', '/api/casiers', 'Ajoute un casier', (req, res) => {
        res.contentType("application/json");
        return casierService.saveOrUpdate(req.body)
            .then((casier) => new Promise((resolve)=> {
                    res.status(200);
                    res.write(JSON.stringify(casier));
                    res.end();
                    resolve();
                })
            )
    });

    app.register('delete', '/api/casiers/:id', 'Suprime un casier', (req, res) => {
        return casierService.remove(req.params.id)
            .then((casier) => new Promise((resolve)=> {
                    res.status(204);
                    res.end();
                    resolve();
                })
            )
    });

    app.register('put', '/api/casiers/:id', 'Met à jour un casier', (req, res) => {
        res.contentType("application/json");
        var casier = req.body;
        casier.id = req.params.id;
        return casierService.saveOrUpdate(casier)
            .then((casier) => new Promise((resolve)=> {
                    res.status(200);
                    res.write(JSON.stringify(casier));
                    res.end();
                    resolve();
                })
            )
    });

    app.register('get', '/api/casiers/:id/bouteilles', 'Liste l\'ensemble des bouteilles d\'un casiers', (req, res) => {
        res.contentType("application/json");
        return casierService.findByCasierId(req.params.id)
            .then((casiers) => new Promise((resolve)=> {
                    res.status(200);
                    res.write(JSON.stringify(casiers));
                    res.end();
                    resolve();
                })
            )
    });

};

module.exports = init;