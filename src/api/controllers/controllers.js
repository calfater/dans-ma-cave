const express = require('express');
const bodyParser = require('body-parser');

var init = (app) => {

    app.help = "";

    app.register = (method, uri, msg, fn) => {
        var line = "";
        line += ("[" + method.toUpperCase() + "]");
        line += new Array(Math.max(0, 10 - line.length)).join(' ');
        line += ("" + uri);
        line += new Array(Math.max(0, 55 - line.length)).join(' ');
        line += (" - " + msg );

        app.help += line + '\n';

        app[method](uri, (req, res) => {
            fn(req, res)
                .catch((e) => new Promise((resolve, reject) => {
                    res.contentType("text/plain");
                    console.log(e);
                    res.status(500);
                    res.write(e.toString());
                    res.end();
                    resolve();
                }))
        });
        return app;
    };

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());

    require('./controller-bouteille')(app);
    require('./controller-casier')(app);
    require('./controller-cepage')(app);
    require('./controller-config')(app);
    require('./controller-region')(app);
    require('./controller-repartition')(app);
    require('./controller-type')(app);
    require('./controller-volume')(app);

    app.register('get', '/api/*', '', function (req, res) {
        return new Promise(resolve=> {
            res.contentType("text/plain");
            res.status(404);
            res.write(app.help);
            res.end();
            resolve();
        })
    });


};

module.exports = init;
