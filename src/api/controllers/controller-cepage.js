const cepageService = require('./../../api/services/cepage-service');

var init = (app) => {
    app.register('get', '/api/cepages', 'Liste tous les cepages', function (req, res) {
        res.contentType("application/json");
        return cepageService.findAll()
            .then((cepages) => {
                res.status(200);
                res.write(JSON.stringify(cepages));
                res.end()
            })
    });
};

module.exports = init;