const typeService = require('./../../api/services/type-service');

var init = (app) => {
    app.register('get', '/api/types', 'Liste tous les types', function (req, res) {
        res.contentType("application/json");
        return typeService.findAll()
            .then((types) => {
                res.status(200);
                res.write(JSON.stringify(types));
                res.end()
            })
    });

};

module.exports = init;