const bouteilleService = require('./../../api/services/bouteille-service');
const regionService = require('./../../api/services/region-service');
const typeService = require('./../../api/services/type-service');

var init = (app) => {
    app.register('get', '/api/repartition', 'Fournit des stats de répartition par type / régions / ...', function (req, res) {
        res.contentType("application/json");
        var resObj = {};
        return new Promise(resolve => resolve())
            .then(bouteilleService.groupByRegionEtType)
            .then(regionEtType => new Promise(resolve => {
                resObj.regionEtType = regionEtType;
                resolve();
            }))
            .then(bouteilleService.groupByApogee)
            .then(apogee => new Promise(resolve => {
                resObj.apogee = apogee;
                resolve();
            }))
            .then(regionService.groupByRegion)
            .then(regions => new Promise(resolve => {
                resObj.region = regions;
                resolve();
            }))
            .then(typeService.groupByType)
            .then(types => new Promise(resolve => {
                resObj.type = types;
                resolve();
            }))
            .then(() => new Promise(resolve => {
                res.status(200);
                res.write(JSON.stringify(resObj));
                res.end();
                resolve();
            }))
    });
};

module.exports = init;