const configService = require('./../../api/services/config-service');

var init = (app) => {
    app.register('get', '/api/configs', 'Liste les clés de config', function (req, res) {
        res.contentType("application/json");
        return configService.findAll()
            .then((config) => new Promise((resolve)=> {
                    res.status(200);
                    res.write(JSON.stringify(config));
                    res.end();
                    resolve();
                })
            )
    });

    app.register('get', '/api/configs/:cle', 'Retourne la valeur d\'une clé', function (req, res) {
        res.contentType("application/json");
        return configService.findValeur(req.params.cle)
            .then((config) => new Promise((resolve)=> {
                    res.status(200);
                    res.write(JSON.stringify(config || null));
                    res.end();
                    resolve();
                })
            )
    });

    app.register('put', '/api/configs', 'Set une config', function (req, res) {
        res.contentType("application/json");
        return configService.set(req.body)
            .then((config) => new Promise((resolve)=> {
                    res.status(204);
                    res.end();
                    resolve();
                })
            )
    });
};

module.exports = init;