const volumeService = require('./../../api/services/volume-service');

var init = (app) => {
    app.register('get', '/api/volumes', 'Liste tous les volumes', function (req, res) {
        res.contentType("application/json");
        return volumeService.findAll()
            .then((volumes) => {
                res.status(200);
                res.write(JSON.stringify(volumes));
                res.end()
            })
    });

};

module.exports = init;