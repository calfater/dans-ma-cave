const bouteilleService = require('./../../api/services/bouteille-service');

var init = (app) => {

    app.register("get", '/api/bouteilles', 'Liste toutes les bouteilles', (req, res) => {
        res.contentType("application/json");
        return bouteilleService.findAll()
            .then((bouteilles) => {
                res.status(200);
                res.write(JSON.stringify(bouteilles));
                res.end()
            })
    });

    app.register('post', '/api/bouteilles', 'Crée une nouvelle bouteille', function (req, res) {
        res.contentType("application/json");
        return bouteilleService.saveOrUpdate(req.body)
            .then(bouteilles => new Promise((resolve)=> {
                    res.status(201);
                    res.write(JSON.stringify(bouteilles));
                    res.end();
                    resolve();
                })
            )
    });

    app.register('get', '/api/bouteilles/:id', 'Récupère une bouteille', function (req, res) {
        res.contentType("application/json");
        return bouteilleService.find(req.params.id)
            //.then((p) => new Promise(r=>setTimeout(() => r(p), 3000)))
            .then((bouteille) => new Promise((resolve)=> {
                    res.status(200);
                    res.write(JSON.stringify(bouteille));
                    res.end();
                    resolve();
                })
            )

    });

    app.register('put', '/api/bouteilles/:id', 'Mets à jour une bouteille', function (req, res) {
        res.contentType("application/json");
        var bouteille = req.body;
        bouteille.id = req.params.id;
        return bouteilleService.saveOrUpdate(bouteille)
            .then(bouteilles => new Promise((resolve)=> {
                    res.status(200);
                    res.write(JSON.stringify(bouteilles));
                    res.end();
                    resolve();
                })
            )
    });

    app.register('delete', '/api/bouteilles/:id', 'Supprime une bouteille', function (req, res) {
        res.contentType("application/json");
        return bouteilleService.remove(req.params.id)
            .then(() => new Promise((resolve)=> {
                    res.status(204);
                    res.end();
                    resolve();
                })
            )

    });

    app.register('delete', '/api/bouteilles/:bId/emplacements/:eId', 'Supprime l\'emplacement d\'une bouteille et décrémente son stock', function (req, res) {
        res.contentType("application/json");
        return bouteilleService.removeEmplacement(req.params.bId, req.params.eId)
            .then(() => new Promise((resolve)=> {
                    res.status(204);
                    res.end();
                    resolve();
                })
            )
    });
};

module.exports = init;