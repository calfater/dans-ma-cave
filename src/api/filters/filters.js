module.exports = (app) => {
    require('./filter-auth')(app);
    require('./filter-static-content')(app);
};

