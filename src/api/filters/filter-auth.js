const basicAuth = require('express-basic-auth');
const md5 = require('md5');

const configService = require('./../../api/services/config-service');

module.exports = (app) => {
    app.use(basicAuth({
        authorizeAsync: true,
        authorizer: async (u, p, cb) => {
            var login = await configService.findValeur("login");
            var password = await configService.findValeur("mot_de_passe");
            cb(null, login === u && password === md5(p));
        },
        challenge: true,
        realm: 'Dans ma cave',
        unauthorizedResponse: req => {
            return req.auth ? 'Credentials rejected' : 'No credentials provided';
        }
    }));

};
