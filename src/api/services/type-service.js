const Type = require('../entities/type');
const Bouteille = require('../entities/bouteille');
const Region = require('../entities/region');
const Sequelize = require('sequelize');

function findAll() {
    return Type.findAll();
}

function groupByType() {
  return Type.findAll({
    attributes: [
      ['nom', 'nom'],
        [Sequelize.fn('sum', Sequelize.fn('coalesce', Sequelize.col('bouteilles.stock'), 0)), 'quantite']
    ],
   include: [
     {model: Bouteille, attributes: []}
   ],
   group: [Sequelize.col('type.nom')]
  });
}

module.exports = {
    findAll: findAll,
    groupByType: groupByType
};
