const Region = require('../entities/region');
const Bouteille = require('../entities/bouteille');
const Sequelize = require('sequelize');

function findAll() {
    return Region.findAll({order: [['nom', 'ASC']]});
}

function groupByRegion() {
    return Region.findAll({
        attributes: [
            ['nom', 'nom'],
            [Sequelize.fn('sum', Sequelize.fn('coalesce', Sequelize.col('bouteilles.stock'), 0)), 'quantite'],
            ['ordre', 'ordre']
        ],
        include: [
            {model: Bouteille, attributes: [], include: [{model: Type, attributes: []}]}
        ],
        group: [Sequelize.col('region.nom')]
    });
}

module.exports = {
    findAll: findAll,
    groupByRegion: groupByRegion
};
