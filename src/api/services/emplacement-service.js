const Emplacement = require('../entities/emplacement');
const Orm = require('../orm');


function remove(id) {
    return Emplacement.findById(id).then((emplacement) => emplacement.destroy());
}


module.exports = {
    remove: remove

};
