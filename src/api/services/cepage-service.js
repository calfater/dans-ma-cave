const Cepage = require('../entities/cepage');

function findAll(regionId) {
    return Cepage.findAll();

}

function getOrCreate(nom) {
    return Cepage.findOne({where: {nom: nom}}).then((dbCepage) => {
            if (null !== dbCepage) {
                return new Promise(resolve => resolve(dbCepage))
            } else {
                return Cepage.create({nom: nom});
            }
        }
    );
}

module.exports = {
    findAll: findAll,
    getOrCreate: getOrCreate
};
