const Bouteille = require('../entities/bouteille');
const Type = require('../entities/type');
const Casier = require('../entities/casier');
const Volume = require('../entities/volume');
const Region = require('../entities/region');
const Cepage = require('../entities/cepage');
const Composition = require('../entities/composition');
const Emplacement = require('../entities/emplacement');

const appellationService = require('./appellation-service');
const emplacementService = require('./emplacement-service');
const cepageService = require('./cepage-service');
const casierService = require('./casier-service');
const configService = require('./config-service');

const Orm = require('../orm');

const https = require('https');

const VOLUME_DE_REFERENCE = 750;
const COEF = 100;

var calculSousIndice = function (valeur, poids, max) {
    return parseInt(valeur * 1 / max * poids * COEF);
};

var calculIndice = async function (bouteilles) {
    var poidsMaturation = await configService.get('poids_maturation');
    var poidsPrix = await configService.get('poids_prix');
    var poidsEtoile = await configService.get('poids_etoile');
    var poidsVolume = await configService.get('poids_volume');
    var indiceEnPourcent = await configService.get('indice_en_pourcent') === "true";

    return new Promise(async resolve => {
        let bouteilleList = [];

        let criteres = await Orm.get().query(
            'SELECT ' +
            '    bouteille.id as id,' +
            '    bouteille.boire_apogee as apogee,' +
            '    bouteille.millesime as millesime,' +
            '    bouteille.prix AS prix,' +
            '    bouteille.etoile AS etoile,' +
            '    volume.volume AS volume' +
            '  FROM bouteilles AS bouteille' +
            '    JOIN volumes as volume on bouteille.volume_id = volume.id' +
            '  WHERE bouteille.est_indicee IS TRUE',
            {type: Orm.get().QueryTypes.SELECT}
        );

        let maxMaturation = 0;
        let maxPrix = 0;
        let maxEtoile = 0;
        let maxVolume = 0;
        for (let critete of criteres) {
            let tempsDeMaturation = Math.max(critete.apogee, new Date().getFullYear()) - critete.millesime;

            maxMaturation = Math.max(maxMaturation, tempsDeMaturation);
            maxPrix = Math.max(maxPrix, critete.prix / critete.volume * VOLUME_DE_REFERENCE);
            maxEtoile = Math.max(maxEtoile, critete.etoile);
            maxVolume = Math.max(maxVolume, critete.volume);
        }

        let maxIndice = 0;
        let indices = {};
        for (let result of criteres) {
            let tempsDeMaturation = Math.max(result.apogee, new Date().getFullYear()) - result.millesime;

            let maturation = calculSousIndice(tempsDeMaturation, poidsMaturation, maxMaturation);
            let etoile = calculSousIndice(result.etoile, poidsEtoile, maxEtoile);
            let prix = calculSousIndice(result.prix / result.volume * VOLUME_DE_REFERENCE, poidsPrix, maxPrix);
            let volume = Math.pow(result.volume / VOLUME_DE_REFERENCE, poidsVolume);

            let indice = parseInt((maturation + etoile + prix ) * volume);

            indices["" + result.id] = {
                indice: indice,
                maturation: maturation,
                etoile: etoile,
                prix: prix,
                volume: volume.toFixed(2)

            };
            maxIndice = Math.max(maxIndice, indice);
        }

        for (let bouteille of bouteilles) {
            let indice = indices["" + bouteille.id];
            if (bouteille.dataValues.est_indicee) {
                bouteilleList.push(Object.assign(bouteille.dataValues, {
                    indice: {
                        valeur: indiceEnPourcent ? parseInt(indice.indice / (maxIndice) * 100) : indice.indice,
                        valeur_pourcent: parseInt(indice.indice / (maxIndice) * 100),
                        detail: {
                            maturation: indiceEnPourcent ? indice.maturation / (maxIndice) * 100 : indice.maturation,
                            etoile: indiceEnPourcent ? indice.etoile / (maxIndice) * 100 : indice.etoile,
                            prix: indiceEnPourcent ? indice.prix / (maxIndice) * 100 : indice.prix,
                            volume: indice.volume
                        }
                    }
                }))
            } else {
                bouteilleList.push(Object.assign(bouteille.dataValues, {
                    indice: {
                        valeur: null,
                        valeur_pourcent: null,
                        detail: {
                            maturation: null,
                            etoile: null,
                            prix: null,
                            volume: null
                        }
                    }
                }));
            }
        }
        resolve(bouteilleList);
    });
};

function findAll() {
    return Bouteille.findAll({include: [{all: true}, {model: Composition, include: [{model: Cepage}]}]})
        .then(calculIndice);
}

async function saveOrUpdate(bouteille) {

    if (!bouteille.nom) {
        return new Promise((resolve, reject)=> reject("Le nom n'est pas valide"));
    }
    if (!bouteille.millesime) {
        return new Promise((resolve, reject)=> reject("Le millesime n'est pas valide"));
    }
    if (!bouteille.type) {
        return new Promise((resolve, reject)=> reject("Le type n'est pas valide"));
    }
    if (!bouteille.region) {
        return new Promise((resolve, reject)=> reject("La région n'est pas valide"));
    }
    if (!bouteille.volumeId) {
        return new Promise((resolve, reject)=> reject("Le volume n'est pas valide"));
    }

    var type = await Type.findOne({where: {nom: bouteille.type}}).then((type) => {
            if (null !== type) {
                return new Promise(resolve => resolve(type))
            } else {
                return new Promise(resolve => resolve(null));
                //return Type.create({nom: bouteille.type.nom});
            }
        }
    );

    if (null === type) {
        return new Promise((resolve, reject)=> reject("Le type n'existe pas"));
    }

    var region = await Region.findOne({where: {nom: bouteille.region}}).then((region) => {
            if (null !== region) {
                return new Promise(resolve => resolve(region))
            } else {
                return new Promise(resolve => resolve(null));
                //return Region.create({nom: bouteille.region.nom});
            }
        }
    );
    if (null === region) {
        return new Promise((resolve, reject)=> reject("La région n'existe pas"));
    }

    var appellation = (bouteille.appellation)
        ? appellation = await appellationService.getOrCreate({region_id: region.id, nom: bouteille.appellation})
        : null;

    var casier = null;
    if (bouteille.casier && bouteille.casier.id) {
        casier = await Casier.findOne({where: {id: bouteille.casier.id}});
    }

    var volume = await Volume.findOne({where: {id: bouteille.volume.id}});

    if (!bouteille.id) {
        return Bouteille.create({
            nom: bouteille.nom,
            type_id: type.id,
            region_id: region.id,
            appellation_id: null !== appellation ? appellation.id : null,
            millesime: bouteille.millesime,
            boire_apres: bouteille.boire_apres || null,
            boire_apogee: bouteille.boire_apogee || null,
            boire_avant: bouteille.boire_avant || null,
            prix: bouteille.prix || null,
            stock: bouteille.stock,
            casier_id: null !== casier ? casier.id : null,
            colonne: bouteille.colonne || null,
            ligne: bouteille.ligne || null,
            description: bouteille.description || null,
            est_indicee: bouteille.est_indicee !== false,
            volume_id: volume.id,
            etoile: bouteille.etoile
        }, {
            //include: [Volume]
        }).then((newBouteille) => new Promise(async resolve => {
            for (let composition of bouteille.compositions) {
                var cepage = await cepageService.getOrCreate(composition.cepage);
                await Composition.create({cepage_id: cepage.id, bouteille_id: newBouteille.id, pourcentage: composition.pourcentage});
            }
            for (let emplacement of bouteille.emplacements) {
                var casier = await casierService.getByNom(emplacement.casier);
                try {
                    await Emplacement.create({casier_id: casier.id, bouteille_id: newBouteille.id, colonne: emplacement.colonne, ligne: emplacement.ligne});
                } catch (e) {
                    reject(emplacement.casier + " [" + emplacement.colonne + "-" + emplacement.ligne + "] déja utilisé");
                    return;
                }
            }
            resolve(Bouteille.findOne({where: {id: newBouteille.id}}));
        }))
    } else {
        return Bouteille.findOne({where: {id: bouteille.id}, include: [{model: Composition}, {model: Emplacement}]})
            .then(existingBouteille => existingBouteille.update({
                nom: bouteille.nom,
                type_id: type.id,
                region_id: region.id,
                appellation_id: null !== appellation ? appellation.id : null,
                millesime: bouteille.millesime,
                boire_apres: bouteille.boire_apres || null,
                boire_apogee: bouteille.boire_apogee || null,
                boire_avant: bouteille.boire_avant || null,
                prix: bouteille.prix || null,
                stock: bouteille.stock,
                casier_id: null !== casier ? casier.id : null,
                colonne: bouteille.colonne || null,
                ligne: bouteille.ligne || null,
                description: bouteille.description || null,
                est_indicee: bouteille.est_indicee !== false,
                volume_id: volume.id,
                etoile: bouteille.etoile
            })).then((existingBouteille) => new Promise(async (resolve, reject) => {
                for (let composition of existingBouteille.compositions) {
                    await composition.destroy();
                }
                for (let composition of bouteille.compositions) {
                    var cepage = await cepageService.getOrCreate(composition.cepage);
                    await Composition.create({cepage_id: cepage.id, bouteille_id: bouteille.id, pourcentage: composition.pourcentage});
                }
                for (let emplacement of existingBouteille.emplacements) {
                    await emplacement.destroy();
                }
                for (let emplacement of bouteille.emplacements) {
                    var casier = await casierService.getByNom(emplacement.casier);
                    try {
                        await Emplacement.create({casier_id: casier.id, bouteille_id: bouteille.id, colonne: emplacement.colonne, ligne: emplacement.ligne});
                    } catch (e) {
                        reject(emplacement.casier + " [" + emplacement.colonne + "-" + emplacement.ligne + "] déja utilisé");
                        return;
                    }
                }

                resolve(Bouteille.findOne({where: {id: bouteille.id}}));
            }));
    }

}

function find(id) {
    return Bouteille.findById(id, {include: [{all: true}, {model: Composition, include: [{model: Cepage}]}, {model: Emplacement, include: [{model: Casier}]}]})
        .then(bouteille => new Promise(async resolve => {
            var googleApiKey = await  configService.get('googleApiKey');
            var googleApiCx = await  configService.get('googleApiCx');
            if (googleApiKey && googleApiCx) {
                var url = 'https://www.googleapis.com/customsearch/v1?key=' + googleApiKey + '&cx=' + googleApiCx + '&searchType=image&q=' + bouteille.nom + " " + bouteille.type.nom + " " + (bouteille.appellation ? bouteille.appellation.nom : "") + " ";
                https.get(url, (ws) => {
                    var jsonStr = "";
                    ws.on('data', (data) => {
                        jsonStr += data
                    });
                    ws.on('end', function () {
                        var b = JSON.parse(JSON.stringify(bouteille));
                        try {
                            b.img = JSON.parse(jsonStr).items[0].link;
                        } catch (e) {
                            console.log(e);
                            console.log(jsonStr);
                        }
                        console.log(JSON.stringify(b));
                        resolve(b);
                    });
                }).on('error', (e) => {
                    console.error(e);
                    calculIndice([bouteille]).then(bouteilles => resolve(bouteilles[0]));
                });
            } else {
                calculIndice([bouteille]).then(bouteilles => resolve(bouteilles[0]));
            }
        }));
}

function remove(id) {
    return Bouteille.findById(id).then((bouteille) => bouteille.destroy());
}

function groupByRegionEtType() {
    return Orm.get().query(
            'SELECT region.nom AS region_nom, type.nom AS type_nom, coalesce(sum(bouteille.stock), 0) AS quantite_totale,  count(bouteille.id) AS quantite_differente' +
            '  FROM regions AS region ' +
            '    JOIN types AS type' +
            '    LEFT JOIN bouteilles AS bouteille ON region.id = bouteille.region_id AND  bouteille.type_id = type.id' +
            '  GROUP BY region.nom, type.nom' +
            '  ORDER BY region.nom, type.nom',
        {type: Orm.get().QueryTypes.SELECT}
        )
        .then((results) => new Promise(resolve => {
            var dtoList = {};
            results.map((v, k) => {
                if ('undefined' === typeof (dtoList[v.region_nom])) {
                    dtoList[v.region_nom] = {};
                }
                dtoList[v.region_nom][v.type_nom] = {quantiteTotale: parseInt(v.quantite_totale) || 0, quantiteDifferente: parseInt(v.quantite_differente) || 0};
            });
            resolve(dtoList);
        }))
        ;
}

function removeEmplacement(bouteilleId, emplacementId) {
    return emplacementService.remove(emplacementId)
        .then(Orm.get().query(
            'UPDATE bouteilles SET stock = (stock - 1) WHERE id = :bouteilleId',
            {replacements: {bouteilleId: bouteilleId}, type: Orm.get().QueryTypes.UPDATE}
        ));
}

function groupByApogee() {
    return Orm.get().query(
            'SELECT bouteille.boire_apogee as apogee, sum(stock) as quantite' +
            '  FROM bouteilles AS bouteille ' +
            '  GROUP BY bouteille.boire_apogee' +
            '  ORDER BY apogee',
        {type: Orm.get().QueryTypes.SELECT}
        )
        .then((results) => new Promise(resolve => {
            let debut = ((results[0] && results[0].apogee) || (results[1] && results[1].apogee) || new Date().getFullYear());
            let fin = (results[results.length - 1] && results[results.length - 1].apogee || new Date().getFullYear());
            for (let annee = debut ; annee <= fin; ++ annee) {
                let found = false;
                results.map((v) => {
                     if (v.apogee === annee) {
                       found = true;
                     };
                 });
                 if (!found) {
                   results.push({apogee: annee, quantite: 0});
                 }
            }
            resolve(results);
        }))
        ;
}

module.exports = {
    findAll: findAll,
    saveOrUpdate: saveOrUpdate,
    find: find,
    remove: remove,
    groupByRegionEtType: groupByRegionEtType,
    removeEmplacement: removeEmplacement,
    groupByApogee: groupByApogee
};
