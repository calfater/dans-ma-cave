const Config = require('../entities/config');
const Orm = require('../orm');

function findAll() {
    return Config.findAll().then(data => new Promise(resolve => {
        var resp = {};
        for (let prop of data) {
            resp[prop.cle] = prop.valeur;
        }
        resolve(resp);
    }));
}

function get(cle) {
    return Config.findOne({where: {cle: cle}}).then(config => new Promise(resolve => {
        if (null !== config) {
            resolve(config.valeur);
        } else {
            resolve(undefined);
        }
    }));
}

function set(configs) {
    return new Promise(async resolve => {
        for (let entry of Object.entries(configs)) {
            var cle = entry[0];
            var valeur = entry[1];
            var values = {cle: cle, valeur: valeur};
            await get(cle).then((dbValeur) => new Promise(async resolve => {
                if ('undefined' === typeof(dbValeur)) {
                    await Config.create(values);
                } else {
                    await Orm.get().query(
                        'UPDATE configs SET valeur = :valeur WHERE cle = :cle',
                        {replacements: values, type: Orm.get().QueryTypes.UPDATE}
                    )
                }
                resolve();
            }));
        }
        resolve();
    });
}

module.exports = {
    findAll: findAll,
    findValeur: get,
    get: get,
    set: set
};
