const Volume = require('../entities/volume');

function findAll() {
    return Volume.findAll({order: [['volume', 'ASC']]});
}

module.exports = {
    findAll: findAll
};