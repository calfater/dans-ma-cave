const Appellation = require('../entities/appellation');
const Region = require('../entities/region');
const Op = require('../orm').Sequelize.Op;

function getOrCreate(appellation) {
    return Appellation.findOne({where: {region_id: appellation.region_id, nom: appellation.nom}}).then((dbAppellation) => {
            if (null !== dbAppellation) {
                return new Promise(resolve => resolve(dbAppellation))
            } else {
                return Appellation.create({region_id: appellation.region_id, nom: appellation.nom});
            }
        }
    );
}

function getByRegionIdOrNom(regionId, nom) {
    return Appellation.findAll({where:{[Op.or]: [{region_id: regionId}, {'$region.nom$': nom}]}, include: [{model: Region}]});
}

module.exports = {
    getOrCreate: getOrCreate,
    getByRegionIdOrNom: getByRegionIdOrNom
};
