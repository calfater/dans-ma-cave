const Casier = require('../entities/casier');
const Bouteille = require('../entities/bouteille');
const Orm = require('../orm');

function findAll() {
    return Casier.findAll({order: [['nom', 'ASC']]});
}

function getByNom(nom) {
    return Casier.findOne({where: {nom: nom}});
}

function findByCasierId(id) {
    //return Casier.findAll({where: {id: id}, include: [{all: true}]});
    return Orm.get().query(
            'SELECT c.nombre_de_colonnes as nombre_de_colonnes,' +
            '    c.nombre_de_lignes as nombre_de_lignes,' +
            '    e.colonne as c, e.ligne as l, e.id as emplacementId,' +
            '    b.id as id, b.nom as nom, b.millesime as millesime,' +
            '    r.nom as region,' +
            '    a.nom as appellation, ' +
            '    t.nom as type' +
            '  from casiers as c ' +
            '    JOIN emplacements as e on c.id = e.casier_id' +
            '    JOIN bouteilles as b on b.id = e.bouteille_id' +
            '    JOIN regions as r on r.id = b.region_id' +
            '    JOIN types as t on t.id = b.type_id' +
            '    LEFT JOIN appellations as a on a.id = b.appellation_id' +
            '  WHERE c.id = :casierId' +
            '    AND e.colonne IS NOT NULL' +
            '    AND e.ligne IS NOT NULL',
        {replacements: {casierId: id}, type: Orm.get().QueryTypes.SELECT}
        )
        .then((results) => new Promise(resolve => {
            var dtoList = {};
            if (results[0]) {
                for (let c = 1; c <= results[0].nombre_de_colonnes; c++) {
                    dtoList[c] = {};
                    for (let l = 1; l <= results[0].nombre_de_lignes; l++) {
                        dtoList[c][l] = null;
                    }
                }
            }
            results.map((v, k) => {
                dtoList[parseInt(v.c)][parseInt(v.l)] = {
                    id: v.id,
                    nom: v.nom,
                    region: v.region,
                    type: v.type,
                    appellation: v.appellation,
                    millesime: v.millesime,
                    emplacementId: v.emplacementId
                };
            });
            resolve(dtoList);
        }))
        ;

}

function saveOrUpdate(casier) {
    if (casier.id) {
        return Casier.findOne({where: {id: parseInt(casier.id)}}).then((existingCasier) => existingCasier.update({
            nom: casier.nom,
            nombre_de_colonnes: casier.nombre_de_colonnes,
            nombre_de_lignes: casier.nombre_de_lignes
        }));
    } else {
        return Casier.create({
            nom: casier.nom,
            nombre_de_colonnes: casier.nombre_de_colonnes,
            nombre_de_lignes: casier.nombre_de_lignes
        })
    }
}

function remove(id) {
    return Casier.findOne({where: {id: id}}).then((casier) => casier.destroy());
}

module.exports = {
    findAll: findAll,
    getByNom: getByNom,
    findByCasierId: findByCasierId,
    saveOrUpdate: saveOrUpdate,
    remove: remove
};
