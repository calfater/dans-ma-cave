CREATE TABLE configs (
  id     INTEGER PRIMARY KEY AUTO_INCREMENT,
  cle    VARCHAR(255) NOT NULL,
  valeur VARCHAR(255) NOT NULL,
  CONSTRAINT unique_cle UNIQUE (cle)
);

CREATE TABLE cepages (
  id  INTEGER PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(255) NOT NULL,
  CONSTRAINT unique_nom UNIQUE (nom)
);

CREATE TABLE IF NOT EXISTS regions (
  id  INTEGER PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(255) NOT NULL,
  CONSTRAINT unique_nom UNIQUE (nom)
);

CREATE TABLE IF NOT EXISTS `types` (
  id  INTEGER PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(255) NOT NULL,
  CONSTRAINT unique_nom UNIQUE (nom)
);

CREATE TABLE IF NOT EXISTS volumes (
  id     INTEGER PRIMARY KEY AUTO_INCREMENT,
  volume INTEGER      NOT NULL,
  nom    VARCHAR(255) NOT NULL,
  CONSTRAINT unique_nom UNIQUE (nom),
  CONSTRAINT unique_volume UNIQUE (volume)
);

CREATE TABLE IF NOT EXISTS casiers (
  id                 INTEGER PRIMARY KEY AUTO_INCREMENT,
  nom                VARCHAR(255) NOT NULL,
  nombre_de_colonnes INTEGER      NOT NULL,
  nombre_de_lignes   INTEGER      NOT NULL,
  CONSTRAINT unique_nom UNIQUE (nom)
);

CREATE TABLE IF NOT EXISTS appellations (
  id        INTEGER PRIMARY KEY AUTO_INCREMENT,
  region_id INTEGER      NOT NULL,
  nom       VARCHAR(255) NOT NULL,
  CONSTRAINT unique_nom_region UNIQUE (nom, region_id),
  --   KEY key_appellations_region_id (region_id),
  CONSTRAINT fk_appellations_region_id FOREIGN KEY (region_id) REFERENCES regions (id)
);

CREATE TABLE IF NOT EXISTS bouteilles (
  id             INTEGER PRIMARY KEY               AUTO_INCREMENT,
  nom            VARCHAR(255) NOT NULL,
  type_id        INTEGER      NOT NULL,
  region_id      INTEGER      NOT NULL,
  appellation_id INTEGER                           DEFAULT NULL,
  millesime      INTEGER      NOT NULL,
  boire_apres    INTEGER                           DEFAULT NULL,
  boire_apogee   INTEGER                           DEFAULT NULL,
  boire_avant    INTEGER                           DEFAULT NULL,
  volume_id      INTEGER      NOT NULL,
  prix           DECIMAL(10, 2)                    DEFAULT NULL,
  stock          INTEGER      NOT NULL             DEFAULT 1,
  etoile         INTEGER                           DEFAULT NULL,
  description    TEXT                              DEFAULT NULL,

  CONSTRAINT unique_bouteille UNIQUE (nom, type_id, region_id, appellation_id, millesime),
  --   KEY key_bouteilles_appellation_id (appellation_id),
  --   KEY key_bouteilles_applellation_id (appellation_id),
  --   KEY key_bouteilles_casier_id (casier_id),
  --   KEY key_bouteilles_region_id (region_id),
  --   KEY key_bouteilles_type_id (type_id),
  --   KEY key_bouteilles_volume_id (volume_id),
  CONSTRAINT fk_bouteilles_applellation_id FOREIGN KEY (appellation_id) REFERENCES appellations (id),
  CONSTRAINT fk_bouteilles_region_id FOREIGN KEY (region_id) REFERENCES regions (id),
  CONSTRAINT fk_bouteilles_type_id FOREIGN KEY (type_id) REFERENCES `types` (id),
  CONSTRAINT fk_bouteilles_volume_id FOREIGN KEY (volume_id) REFERENCES volumes (id)
);

CREATE TABLE IF NOT EXISTS compositions (
  id           INTEGER PRIMARY KEY AUTO_INCREMENT,
  bouteille_id INTEGER NOT NULL,
  cepage_id    INTEGER NOT NULL,
  pourcentage  INTEGER NOT NULL,
  CONSTRAINT unique_composition UNIQUE (bouteille_id, cepage_id),
  --   KEY key_composition_sepage_id (cepage_id),
  --   KEY key_composition_bouteille_id (bouteille_id),
  CONSTRAINT fk_composition_bouteille_id FOREIGN KEY (bouteille_id) REFERENCES bouteilles (id),
  CONSTRAINT fk_composition_sepage_id FOREIGN KEY (cepage_id) REFERENCES cepages (id)
);

CREATE TABLE IF NOT EXISTS emplacements (
  id           INTEGER PRIMARY KEY AUTO_INCREMENT,
  bouteille_id INTEGER NOT NULL,
  casier_id    INTEGER NOT NULL,
  colonne      INTEGER             DEFAULT NULL,
  ligne        INTEGER             DEFAULT NULL,
  CONSTRAINT unique_emplacement_casier_colonne_ligne UNIQUE (casier_id, colonne, ligne),
  --   KEY key_emplacement_casier_id (casier_id),
  --   KEY key_emplacement_bouteille_id (bouteille_id),
  CONSTRAINT emplacement_bouteille_id FOREIGN KEY (bouteille_id) REFERENCES bouteilles (id),
  CONSTRAINT emplacement_casier_id FOREIGN KEY (casier_id) REFERENCES casiers (id)
);
