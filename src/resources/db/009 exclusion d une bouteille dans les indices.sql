CREATE TABLE bouteilles_new (
  id             INTEGER        PRIMARY KEY          AUTO_INCREMENT,
  nom            VARCHAR(255)   NOT NULL,
  type_id        INTEGER        NOT NULL,
  region_id      INTEGER        NOT NULL,
  appellation_id INTEGER                             DEFAULT NULL,
  millesime      INTEGER        NOT NULL,
  boire_apres    INTEGER                             DEFAULT NULL,
  boire_apogee   INTEGER                             DEFAULT NULL,
  boire_avant    INTEGER                             DEFAULT NULL,
  volume_id      INTEGER        NOT NULL,
  prix           DECIMAL(10, 2)                      DEFAULT NULL,
  stock          INTEGER        NOT NULL             DEFAULT 1,
  etoile         INTEGER                             DEFAULT NULL,
  est_indicee    BOOLEAN        NOT NULL             DEFAULT TRUE,
  description    TEXT                                DEFAULT NULL,

  CONSTRAINT unique_bouteille UNIQUE (nom, type_id, region_id, appellation_id, millesime),
  CONSTRAINT fk_bouteilles_applellation_id FOREIGN KEY (appellation_id) REFERENCES appellations (id),
  CONSTRAINT fk_bouteilles_region_id FOREIGN KEY (region_id) REFERENCES regions (id),
  CONSTRAINT fk_bouteilles_type_id FOREIGN KEY (type_id) REFERENCES `types` (id),
  CONSTRAINT fk_bouteilles_volume_id FOREIGN KEY (volume_id) REFERENCES volumes (id)
);

INSERT INTO bouteilles_new (id, nom, type_id, region_id, appellation_id, millesime, boire_apres, boire_apogee, boire_avant, volume_id, prix, stock, description, etoile)
  SELECT id, nom, type_id, region_id, appellation_id, millesime, boire_apres, boire_apogee, boire_avant, volume_id, prix, stock, description, etoile FROM bouteilles;

DROP TABLE bouteilles;

ALTER TABLE bouteilles_new RENAME TO bouteilles;

