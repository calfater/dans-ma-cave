ALTER TABLE types ADD COLUMN ordre INT(11);
UPDATE types SET ordre = 1;
UPDATE types SET ordre = 2 WHERE nom= 'Blanc';
UPDATE types SET ordre = 3 WHERE nom= 'Rosé';
UPDATE types SET ordre = 4 WHERE nom= 'Effervescent';